import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<Student> studentArrayList = new ArrayList<>();
        int t = Integer.parseInt(sc.nextLine());
        while (t-- > 0) {
            Student student = new Student(sc.nextLine(), sc.nextLine(), sc.nextLine(), sc.nextLine());
            studentArrayList.add(student);
        }

        int t2 = Integer.parseInt(sc.nextLine());
        while (t2-- > 0) {
            String major = sc.nextLine().toUpperCase();
            String cmp = "";
            if (major.compareTo("KE TOAN") == 0) {
                cmp = "DCKT";
            }
            if (major.compareTo("CONG NGHE THONG TIN") == 0) {
                cmp = "DCCN";
            }
            if (major.compareTo("AN TOAN THONG TIN") == 0) {
                cmp = "DCAT";
            }
            if (major.compareTo("VIEN THONG") == 0) {
                cmp = "DCVT";
            }
            if (major.compareTo("DIEN TU") == 0) {
                cmp = "DCDT";
            }
            System.out.printf("DANH SACH SINH VIEN NGANH %s:\n", major);

            for (Student student : studentArrayList) {
                if (cmp.equals("DCAT") || cmp.equals("DCCN")) {
                    if (student.getGrade().charAt(0) != 'E' && student.getId().substring(3, 7).toUpperCase().equals(cmp)) {
                        {
                            System.out.println(student);
                        }
                    }
                } else {
                    if (student.getId().substring(3, 7).toUpperCase().equals(cmp)) {
                        System.out.println(student);
                    }
                }
            }
        }
    }
}
