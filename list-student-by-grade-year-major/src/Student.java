public class Student {
    private String name;
    private String id;
    private String grade;
    private String gmail;

    public Student(String id, String name, String grade, String gmail) {
        this.name = name;
        this.id = id;
        this.grade = grade;
        this.gmail = gmail;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + grade + " " + gmail;
    }

    public String getGrade() {
        return grade;
    }

    public String getId() {
        return id;
    }
}

