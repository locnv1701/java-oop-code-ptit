public class ThiSinh implements Comparable<ThiSinh> {
    private Integer ma;
    private String name;
    private String ngaySinh;
    private double tongDiem;

    public ThiSinh(int count, String name, String ngaySinh, double tongDiem) {
        this.ma = count;
        this.name = name;
        this.ngaySinh = ngaySinh;
        this.tongDiem = tongDiem;
    }

    public double getTongDiem() {
        return tongDiem;
    }

    @Override
    public String toString() {
        return ma + " " + name + ' ' + ngaySinh + ' ' + tongDiem;
    }

    @Override
    public int compareTo(ThiSinh o) {
        return ma.compareTo(o.ma);
    }
}
