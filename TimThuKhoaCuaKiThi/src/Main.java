import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        ArrayList<ThiSinh> dsts = new ArrayList<>();
        double max = 0;
        int count = 0;
        while (t-- > 0) {
            count++;
            String ten = sc.nextLine();
            String ngaySinh = sc.nextLine();
            Double tongDiem = Double.parseDouble(sc.nextLine()) + Double.parseDouble(sc.nextLine()) + Double.parseDouble(sc.nextLine());
            if (tongDiem > max)
                max = tongDiem;
            dsts.add(new ThiSinh(count, ten, ngaySinh, tongDiem));
        }
        dsts.sort(null);
        double finalMax = max;
        dsts.forEach(x -> {
            if(x.getTongDiem() == finalMax)
                System.out.println(x);

        });

    }
}
