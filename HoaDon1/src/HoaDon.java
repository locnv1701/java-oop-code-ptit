public class HoaDon {
    private String mHD;
    private MatHang sanpham;
    private int soluong;

    public HoaDon(String mHD, int count, MatHang sanpham, int soluong) {
        this.mHD = mHD + String.format("-%03d", count);
        this.sanpham = sanpham;
        this.soluong = soluong;
    }

    private int tinhTien() {
        int tien = mHD.charAt(2) == '1' ? soluong * sanpham.getGia1() : soluong * sanpham.getGia2();
        if (soluong >= 150)
            return tien / 2;
        else if (soluong >= 100)
            return tien * 70 / 100;
        else if (soluong >= 50)
            return tien * 85 / 100;
        else return tien;
    }

    private int tinhGiamTien() {
        int tien = mHD.charAt(2) == '1' ? soluong * sanpham.getGia1() : soluong * sanpham.getGia2();
        return tien - tinhTien();
    }

    @Override
    public String toString() {
        return mHD + ' ' + sanpham.getTen() + " " + tinhGiamTien() + " " + tinhTien();

    }
}
