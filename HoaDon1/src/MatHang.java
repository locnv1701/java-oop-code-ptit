public class MatHang {
    private String maMH;
    private String ten;
    private int gia1;
    private int gia2;


    public MatHang(String maMH, String ten, int gia1, int gia2) {
        this.maMH = maMH;
        this.ten = ten;
        this.gia1 = gia1;
        this.gia2 = gia2;
    }

    public String getMaMH() {
        return maMH;
    }

    public String getTen() {
        return ten;
    }

    public int getGia1() {
        return gia1;
    }

    public int getGia2() {
        return gia2;
    }
}
