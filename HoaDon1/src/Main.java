import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    static ArrayList<MatHang> dsmh = new ArrayList<>();
    static ArrayList<HoaDon> dshd = new ArrayList<>();


    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("DATA1.in"));
        int t = Integer.parseInt(sc.nextLine());
        while (t-- > 0) {
            MatHang matHang = new MatHang(sc.nextLine(), sc.nextLine(), Integer.parseInt(sc.nextLine()), Integer.parseInt(sc.nextLine()));
            dsmh.add(matHang);
        }
        sc.close();
        sc = new Scanner(new File("DATA2.in"));
        t = Integer.parseInt(sc.nextLine());
        int count = 0;
        while (t-- > 0) {
            count++;
            String maMH = sc.next();
            MatHang matHang = null;
            for (MatHang a : dsmh) {
                if (maMH.substring(0, 2).equals(a.getMaMH())) {
                    matHang = new MatHang(a.getMaMH(), a.getTen(), a.getGia1(), a.getGia2());
                    break;
                }
            }
            HoaDon hoaDon = new HoaDon(maMH, count, matHang, Integer.parseInt(sc.next()));
            dshd.add(hoaDon);
        }
        dshd.forEach(System.out::println);
    }
}
