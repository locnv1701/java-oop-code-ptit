import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class WordSet {
    private final String fileName;
    private Set<String> set = new HashSet<>();

    public WordSet(String fileName) {
        this.fileName = fileName;
    }

    private Set<String> solve() throws FileNotFoundException {
        Scanner sc = new Scanner(new File(fileName));
        while(sc.hasNextLine()){
            String line = sc.nextLine();
            String[] words = line.toLowerCase().split("\\s+");
            for (String word : words) {
                set.add(word);
            }
        }
        return set;
    }

    public String union(WordSet s2) throws FileNotFoundException {
        Set<String> setUnion = new HashSet<>();
        setUnion.addAll(this.solve());
        setUnion.addAll(s2.solve());
        List<String> res = new ArrayList<>(setUnion);
        Collections.sort(res);
        String result = "";
        for (String ele : res) {
            result += ele;
            result += " ";
        }
        return result;
    }

    public String intersection(WordSet s2) throws FileNotFoundException {
        Set<String> set = new HashSet<>(this.solve());
        Set<String> res = new HashSet<>();
        s2.solve().forEach(a -> {
            if(set.contains(a))
                res.add(a);
        });
        List<String> list = new ArrayList<>(res);
        Collections.sort(list);
        String result = "";
        for (String ele : list) {
            result += ele;
            result += " ";
        }
        return result;
    }

}
