public class Nhom implements Comparable<Nhom> {
    private SinhVien sinhVien;
    private BaiTap baiTap;

    public Nhom(SinhVien sinhVien, BaiTap baiTap) {
        this.sinhVien = sinhVien;
        this.baiTap = baiTap;
    }

    @Override
    public String toString() {
        return sinhVien.getMaSV() + " " + sinhVien.getTen() + " " + sinhVien.getStd() + " " + baiTap.getStt() + " " + baiTap.getNoiDung();
    }

    @Override
    public int compareTo(Nhom o) {
        return sinhVien.getMaSV().compareTo(o.sinhVien.getMaSV());
    }
}
