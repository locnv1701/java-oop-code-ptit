import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static ArrayList<SinhVien> dssv = new ArrayList<>();
    public static ArrayList<BaiTap> dsbt = new ArrayList<>();
    public static ArrayList<Nhom> dsn = new ArrayList<>();

    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc1 = new Scanner(new File("SINHVIEN.in"));


        int t1 = Integer.parseInt(sc1.nextLine());
        while (t1-- > 0) {
            dssv.add(new SinhVien(sc1.nextLine(), sc1.nextLine(), sc1.nextLine()));
        }
        sc1.close();

        sc1 = new Scanner(new File("BAITAP.in"));
        int t2 = Integer.parseInt(sc1.nextLine());
        int count = 0;
        while (t2-- > 0) {
            count++;
            dsbt.add(new BaiTap(count, sc1.nextLine()));
        }
        sc1.close();

        sc1 = new Scanner(new File("NHOM.in"));
        while (sc1.hasNext()) {
            String maSV = sc1.next();
            Integer maBT = Integer.parseInt(sc1.next());

            Nhom nhom = new Nhom(dssv.stream().filter(sv -> sv.getMaSV().equals(maSV)).findFirst().get(),
                                 dsbt.stream().filter(bt -> bt.getStt().equals(maBT)).findFirst().get());
            dsn.add(nhom);
        }
        dsn.sort(null);
        dsn.forEach(System.out::println);
    }
}
