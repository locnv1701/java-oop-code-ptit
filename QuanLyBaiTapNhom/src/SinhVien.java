public class SinhVien {
    private String maSV;
    private String ten;
    private String std;

    public SinhVien(String maSV, String ten, String std) {
        this.maSV = maSV;
        this.ten = ten;
        this.std = std;
    }

    public String getTen() {
        return ten;
    }

    public String getStd() {
        return std;
    }

    public String getMaSV() {
        return maSV;
    }
}
