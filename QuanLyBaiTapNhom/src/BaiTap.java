public class BaiTap {
    private Integer stt;
    private String noiDung;

    public BaiTap(Integer stt, String noiDung) {
        this.stt = stt;
        this.noiDung = noiDung;
    }

    public String getNoiDung() {
        return noiDung;
    }

    public Integer getStt() {
        return stt;
    }
}
