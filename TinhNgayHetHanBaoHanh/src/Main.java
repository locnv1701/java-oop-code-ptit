import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException, ParseException {
        Scanner sc = new Scanner(new File("MUAHANG.in"));
        ArrayList<SanPham> dssp = new ArrayList<>();
        ArrayList<KhachHang> dskh = new ArrayList<>();

        int t = Integer.parseInt(sc.nextLine());
        while (t-- > 0) {
            dssp.add(new SanPham(sc.nextLine(), sc.nextLine(), Integer.parseInt(sc.nextLine()), Integer.parseInt(sc.nextLine())));
        }
        t = Integer.parseInt(sc.nextLine());
        int count = 0;
        while (t-- > 0) {
            count++;
            String ten = sc.nextLine();
            String diaChi = sc.nextLine();
            String maSP = sc.nextLine();
            SanPham sanPham = dssp.stream().filter(s -> s.getMaSP().equals(maSP)).findFirst().get();
            int soLuong = Integer.parseInt(sc.nextLine());
            String ngayMua = sc.nextLine();
            dskh.add(new KhachHang(
                    String.format("KH%02d", count),
                    ten,
                    diaChi,
                    sanPham,
                    soLuong,
                    ngayMua
            ));
        }
        dskh.sort(null);
        dskh.forEach(System.out::println);
    }
}
