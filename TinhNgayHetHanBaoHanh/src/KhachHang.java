import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class KhachHang implements Comparable<KhachHang> {
    private String maKH;
    private String ten;
    private String diaChi;
    private SanPham sanPham;
    private int soLuong;
    private Date ngayMua;
    private String ngay;

    public KhachHang(String maKH, String ten, String diaChi, SanPham sanPham, int soLuong, String ngayMua) throws ParseException {
        this.maKH = maKH;
        this.ten = ten;
        this.diaChi = diaChi;
        this.sanPham = sanPham;
        this.soLuong = soLuong;
        this.ngayMua = new SimpleDateFormat("dd/MM/yyyy").parse(ngayMua);
        this.ngay = ngayMua.substring(0,2);
    }

    public String getDayEnd() {
        int month = ngayMua.getMonth() + 1 + sanPham.getSoThang();
        int year = ngayMua.getYear() + 1900;
        while (month > 12) {
            month -= 12;
            year += 1;
        }
        return  ngay + "/" + String.format("%02d", month) + "/" + year;
    }

    private String getDayToSort(){
        String tmp = getDayEnd();
        return tmp.substring(6)+ tmp.substring(3,5) + tmp.substring(0,2);
    }


    private Date getHan(){
        Calendar c1 = Calendar.getInstance();
        c1.setTime(ngayMua);
        c1.roll(Calendar.MONTH, sanPham.getSoThang()%12);
        c1.roll(Calendar.YEAR, sanPham.getSoThang()/12);

        return c1.getTime();
    }

    @Override
    public String toString() {
//        return maKH + ' ' + ten + ' ' + diaChi + ' ' + sanPham.getMaSP() + " " + sanPham.getGia() * soLuong + " " + new SimpleDateFormat("dd/MM/yyyy").format(getHan());
        return maKH + ' ' + ten + ' ' + diaChi + ' ' + sanPham.getMaSP() + " " + sanPham.getGia() * soLuong + " " + getDayEnd();
    }

    @Override
    public int compareTo(KhachHang o) {
        int cmp = getDayToSort().compareTo(o.getDayToSort());
        if (cmp == 0) {
            return maKH.compareTo(o.maKH);
        }
        return cmp;

    }
}
