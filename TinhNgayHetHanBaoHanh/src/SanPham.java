public class SanPham {
    private String maSP;
    private String ten;
    private Integer gia;
    private Integer soThang;

    public SanPham(String maSP, String ten, Integer gia, Integer soThang) {
        this.maSP = maSP;
        this.ten = ten;
        this.gia = gia;
        this.soThang = soThang;
    }

    public String getMaSP() {
        return maSP;
    }

    public Integer getGia() {
        return gia;
    }

    public Integer getSoThang() {
        return soThang;
    }
}
