import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        int count = 0;
        ArrayList<Double> gpaList = new ArrayList<>();
        ArrayList<Student> studentArrayList = new ArrayList<>();
        while (t-- > 0) {
            count++;
            String name = sc.nextLine();
            double gpa = Double.parseDouble(sc.nextLine());
            gpaList.add(gpa);
            Student student = new Student(count, name, gpa);
            studentArrayList.add(student);
        }
//        gpaList.sort(null);
        int ranking = 1;
        for (Student student : studentArrayList) {
            ranking = 1;
            for (Double aDouble : gpaList) {
                if(aDouble > student.getGpa())
                    ranking ++;
            }
            student.setRanking(ranking);
            System.out.println(student);
        }
    }
}
