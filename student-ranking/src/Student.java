public class Student {
    private String id;
    private String name;
    private double gpa;
    private String graded;
    private int ranking;

    public Student(int id, String name, double gpa) {
        this.id = "HS" + String.format("%02d", id);
        this.name = name;
        this.gpa = gpa;
        String graded;
        if (gpa < 5)
            graded = "Yeu";
        else if (gpa < 7)
            graded = "Trung Binh";
        else if (gpa < 9)
            graded = "Kha";
        else graded = "Gioi";
        this.graded = graded;
    }

    public double getGpa() {
        return gpa;
    }

    public void setRanking(int ranking) {
        this.ranking = ranking;
    }

    @Override
    public String toString() {
        return id + ' ' + name + ' ' + gpa + ' ' + graded + ' ' + ranking;
    }
}
