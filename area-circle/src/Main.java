import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t-- > 0) {
            Point p1 = new Point(sc.nextDouble(), sc.nextDouble());
            Point p2 = new Point(sc.nextDouble(), sc.nextDouble());
            Point p3 = new Point(sc.nextDouble(), sc.nextDouble());
            if (p1.distance(p2) + p1.distance(p3) <= p2.distance(p3)
                    || p2.distance(p1) + p2.distance(p3) <= p1.distance(p3)
                    || p3.distance(p1) + p3.distance(p2) <= p1.distance(p2)) {
                System.out.println("INVALID");
            }
            else{
                double a = p1.distance(p2);
                double b = p2.distance(p3);
                double c = p1.distance(p3);
                double s = Math.sqrt((a+b+c)*(a+b-c)*(b+c-a)*(a+c-b))/4;
                double R = a*b*c/4/s;
                double area = Math.PI*R*R;
                area = Math.round(area*1000.0)/1000.0;
                System.out.printf("%.3f", area);
                System.out.println();
            }
        }

    }
}
