import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("In.txt"));
        ArrayList<NhanVien> dsnv = new ArrayList<>();
        while (sc.hasNextLine()) {
            String[] input = sc.nextLine().split(";\\s*");
            dsnv.add(new NhanVien(input[0], input[1], Integer.parseInt(input[2]), Integer.parseInt(input[3]), Double.parseDouble(input[4])));
        }
        try {
            FileWriter fw = new FileWriter("Out.txt");
            for (NhanVien nhanVien : dsnv) {
                fw.write(nhanVien.toString());
                fw.write("\n");
            }
            fw.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println("Success...");
    }
}
