public class NhanVien {
    private String ma, ten;
    private int luong, heso;
    private double thue;
    private int tongLuong;

    public NhanVien(String ma, String ten, int luong, int heso, double thue) {
        this.ma = ma;
        this.ten = ten;
        this.luong = luong;
        this.heso = heso;
        this.thue = thue;
        this.tongLuong = (int) (luong*heso*(1-thue));
    }

    @Override
    public String toString() {
        return ma + ';' + ten + ';' +tongLuong;
    }
}
