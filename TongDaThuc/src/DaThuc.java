import java.util.Arrays;

public class DaThuc {
    private int[] heSo;

    public DaThuc(String input) {
        this.heSo = new int[10006];
        Arrays.fill(heSo, 0);
        String[] arr = input.replace("*x^", " ").replace("+", " ").trim().split("\\s+");
        for (int i = 0; i < arr.length; i += 2) {
            heSo[Integer.parseInt(arr[i + 1])] = Integer.parseInt(arr[i]);
        }
    }

    public DaThuc() {
        this.heSo = new int[10006];
        Arrays.fill(heSo, 0);
    }

    public DaThuc cong(DaThuc q) {
        DaThuc r = new DaThuc();
        for (int i = 0; i < 10006; i++) {
            r.heSo[i] = heSo[i] + q.heSo[i];
        }
        return r;
    }

    @Override
    public String toString() {
        String res = "";
        for (int i = 10005; i >= 0; i--) {
            if (heSo[i] != 0)
                res += heSo[i] + "*x^" + i + " + ";
        }
        return res.substring(0, res.length() - 3);
    }
}