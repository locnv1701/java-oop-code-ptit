import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        while (t-- > 0) {
            String[] input = sc.nextLine().split("\\s+");
            System.out.println(new DonHang(input[0], Integer.parseInt(input[1]), Integer.parseInt(input[2])));
        }
    }
}
