public class DonHang {
    private String ma;
    private int donGia;
    private int soLuong;

    public DonHang(String ma, int donGia, int soLuong) {
        this.ma = ma;
        this.donGia = donGia;
        this.soLuong = soLuong;
    }

    private double tien() {
        return donGia * soLuong;
    }

    private double thue() {
        if (ma.charAt(0) == 'T')
            return tien() * 0.29;
        if (ma.charAt(0) == 'C')
            return tien() * 0.1;
        if (ma.charAt(0) == 'D')
            return tien() * 0.08;
        else
            return tien() * 0.02;
    }

    private double phiVanChuyen() {
        if (ma.charAt(0) == 'T')
            return tien() * 0.04;
        if (ma.charAt(0) == 'C')
            return tien() * 0.03;
        if (ma.charAt(0) == 'D')
            return tien() * 0.025;
        else
            return tien() * 0.005;
    }

    private double thanhTien() {
        if (ma.charAt(ma.length() - 1) == 'C')
            return (tien() + phiVanChuyen() + thue() * 0.95) * 1.2 / soLuong;
        else
            return (tien() + phiVanChuyen() + thue()) * 1.2 / soLuong;
    }

    @Override
    public String toString() {
        return ma + ' ' + String.format("%.2f", thanhTien());
    }
}
