import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static ArrayList<LoaiPhong> ds = new ArrayList<>();
    public static void main(String[] args) throws IOException, ParseException {

        Scanner in = new Scanner(new File("DATA.in"));
        int n = Integer.parseInt(in.nextLine());
        while (n-- > 0) {
            ds.add(new LoaiPhong(in.nextLine()));
        }

        ArrayList<Khach> dsk = new ArrayList<>();
        n = Integer.parseInt(in.nextLine());
        int count = 0;
        while (n-- > 0) {
            count++;
            dsk.add(new Khach(count, in.nextLine(), in.nextLine(), in.nextLine(), in.nextLine()));
        }
        dsk.sort(null);
        dsk.forEach(System.out::println);
    }
}
