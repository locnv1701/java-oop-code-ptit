public class LoaiPhong implements Comparable<LoaiPhong> {
    private String kyHieu;
    private String ten;
    private int donGia;
    private double phi;

    public LoaiPhong(String nextLine) {
        String[] word = nextLine.trim().split("\\s+");
        this.kyHieu = word[0];
        this.ten = word[1];
        this.donGia = Integer.parseInt(word[2]);
        this.phi = Double.parseDouble(word[3]);
    }

    public String getKyHieu() {
        return kyHieu;
    }

    public double getPhi() {
        return phi;
    }

    public int getDonGia() {
        return donGia;
    }

    @Override
    public String toString() {
        return kyHieu + ' ' + ten + ' ' + donGia + " " + phi;
    }

    @Override
    public int compareTo(LoaiPhong o) {
        return ten.compareTo(o.ten);
    }
}
