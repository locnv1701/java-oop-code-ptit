import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Khach implements Comparable<Khach> {
    private String maKhach;
    private String ten;
    private String maPhong;
    private LoaiPhong loaiPhong;
    private Date ngayDen;
    private Date ngayDi;

    public Khach(int maKhach, String ten, String maPhong, String ngayDen, String ngayDi) throws ParseException {
        this.maKhach = String.format("KH%02d", maKhach);
        this.ten = ten;
        this.maPhong = maPhong;
        this.loaiPhong = Main.ds.stream().filter(s -> s.getKyHieu().equals(maPhong.substring(2, 3))).findFirst().get();
        this.ngayDen = new SimpleDateFormat("dd/MM/yyyy").parse(ngayDen);
        this.ngayDi = new SimpleDateFormat("dd/MM/yyyy").parse(ngayDi);
    }

    private Long getNgay() {
        return (ngayDi.getTime() - ngayDen.getTime()) / 86400000;
    }

    private double tinhTien() {
        long ngay = getNgay() == 0 ? 1 : getNgay();
        double kq = ngay * loaiPhong.getDonGia() * discount() * (1 + loaiPhong.getPhi());
        return (double) Math.round(kq * 100) / 100;
    }

    private double discount() {
        long ngay = getNgay();
        if (ngay < 10)
            return 1.0;
        else if (ngay < 20)
            return 0.98;
        else if (ngay < 30)
            return 0.96;
        else return 0.94;
    }

    @Override
    public String toString() {
        return maKhach + ' ' + ten + ' ' + maPhong + ' ' + getNgay() + " " + String.format("%.2f", tinhTien());
    }

    @Override
    public int compareTo(Khach o) {
        return o.getNgay().compareTo(getNgay());
    }
}
