import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws ParseException {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String tmp = sc.nextLine();

        List<Racer> listRacer = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            String name = sc.nextLine();
            String address = sc.nextLine();
            String finish = sc.nextLine();
            Racer racer = new Racer(name, finish, address);
            listRacer.add(racer);
        }

        listRacer.sort(null);
        for (Racer racer : listRacer) {
            racer.display();
        }
    }
}

