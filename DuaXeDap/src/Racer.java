import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.lang.Math.round;

public class Racer implements Comparable<Racer> {
    private String iD;
    private String name;
    private Date finish;
    private String address;
    private float time;

    public Racer() {

    }

    public Racer(String name, String finish, String address) throws ParseException {
        String[] token = name.split(" ");
        String res = "";
        for (int i = 0; i < token.length; i++) {
            res += token[i].charAt(0);
        }
        String[] token1 = address.split(" ");
        String res1 = "";
        for (int i = 0; i < token1.length; i++) {
            res1 += token1[i].charAt(0);
        }
        this.iD = res1 + res;
        this.name = name;
        this.address = address;
        Date tmp = new SimpleDateFormat("H:mm").parse(finish);
        this.finish = tmp;
        Date start = new SimpleDateFormat("H:mm").parse("6:00");
        this.time = (float) ((this.finish.getTime() - start.getTime()) / 60000.0);
    }

    public Float getSpeed() {
        return 120 * 60 / this.time;
    }


    public void display() {
        System.out.print(iD + " " + name + " " + address + " " + round(getSpeed()) + " Km/h");
        System.out.println();
    }

    @Override
    public int compareTo(Racer o) {
        Float a = o.getSpeed();
        Float b = this.getSpeed();
        return a.compareTo(b);
    }
}
