public class Staff implements Comparable<Staff>{
    private int id;
    private String name;
    private String position;
    private int basicSalary;
    private int numberOfWorkdays;

    public Staff(int id, String name, String position, int basicSalary, int numberOfWorkdays) {
        this.id = id;
        this.name = name;
        this.position = position;
        this.basicSalary = basicSalary;
        this.numberOfWorkdays = numberOfWorkdays;
    }

    public String getPosition() {
        return position;
    }

    private Integer getSalaryTotal(){
        int bonus = 0;
        if(position.equals("GD"))
            bonus = 500;
        else if(position.equals("PGD"))
            bonus = 400;
        else if(position.equals("TP"))
            bonus = 300;
        else if(position.equals("KT"))
            bonus = 250;
        else bonus = 100;
        int salary = (basicSalary*numberOfWorkdays+bonus);
        return salary;
    }

    public void display(){
        int bonus = 0;
        if(position.equals("GD"))
            bonus = 500;
        else if(position.equals("PGD"))
            bonus = 400;
        else if(position.equals("TP"))
            bonus = 300;
        else if(position.equals("KT"))
            bonus = 250;
        else bonus = 100;
        int salary = (basicSalary*numberOfWorkdays+bonus);
        int tmp =salary *2/3;
        int tamUng;
        if(tmp < 25000){
            tamUng = (int)(Math.round( tmp / 1000.0) * 1000);
        }
        else
            tamUng = 25000;

        System.out.println("NV" + String.format("%02d",id) + " " + name + " " + bonus + " " + basicSalary*numberOfWorkdays + " " + tamUng + " " + (salary - tamUng));
    }

    @Override
    public int compareTo(Staff o) {
        if(getSalaryTotal().compareTo(o.getSalaryTotal()) != 0){
            return o.getSalaryTotal().compareTo(getSalaryTotal());
        }
        Integer a = id;
        Integer b = o.id;
        return a.compareTo(b);
    }
}
