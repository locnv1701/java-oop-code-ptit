import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException, ParseException {
        Scanner scanner = new Scanner(new File("KHACHHANG.in"));

        int numOfStu = Integer.parseInt(scanner.nextLine());

        List<SinhVien> sinhViens = new ArrayList<>();
        int count = 0;
        while (numOfStu-- > 0) {
            count++;
            sinhViens.add(new SinhVien(count, scanner.nextLine(), scanner.nextLine(), scanner.nextLine(), scanner.nextLine()));
        }

        sinhViens.sort(null);
        sinhViens.forEach(System.out::println);
    }
}