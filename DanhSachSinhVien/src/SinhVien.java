import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SinhVien implements Comparable<SinhVien>{
    private String maSV;
    private String hoTen;
    private String lop;
    private String email;
    private Date date;

    public SinhVien(int maSV, String hoTen, String lop, String date, String email) throws ParseException {
        this.maSV = String.format("KH%03d", maSV);
        this.hoTen = chuanHoaHoTen(hoTen);
        this.lop = lop;
        this.email = email;
        this.date = new SimpleDateFormat("dd/MM/yyyy").parse(date);
    }

    public String chuanHoaHoTen(String hoTen) {
        String[] hoTenArr = hoTen.trim().split("\\s+");
        StringBuilder hoTenSb = new StringBuilder();

        for (String ten : hoTenArr) {
            hoTenSb.append(ten.substring(0, 1).toUpperCase()).append(ten.substring(1).toLowerCase()).append(" ");
        }

        return hoTenSb.toString().trim();
    }

    @Override
    public String toString() {
        return maSV + " " + hoTen + " " + lop + " " + email + " " + new SimpleDateFormat("dd/MM/yyyy").format(date);
    }

    @Override
    public int compareTo(SinhVien o) {
        return date.compareTo(o.date);
    }
}

