public class GiangVien {
    public String maGV;
    public String ten;

    public GiangVien(String maGV, String ten) {
        this.maGV = maGV;
        this.ten = ten;
    }

    public String getMaGV() {
        return maGV;
    }

    public String getTen() {
        return ten;
    }
}
