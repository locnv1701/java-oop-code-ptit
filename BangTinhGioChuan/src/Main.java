import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<Lop> dsl = new ArrayList<>();
        ArrayList<GiangVien> dsgv = new ArrayList<>();

        int t = Integer.parseInt(sc.nextLine());
        while (t-- > 0) {
            String[] input = sc.nextLine().split("\\s+");
            String maLop = input[0];
            StringBuilder tenLop = new StringBuilder();
            for (int i = 1; i < input.length; i++) {
                tenLop.append(input[i]);
                tenLop.append(" ");
            }
            dsl.add(new Lop(maLop, tenLop.toString().trim()));
        }
        t = Integer.parseInt(sc.nextLine());
        while (t-- > 0) {
            String[] input = sc.nextLine().split("\\s+");
            String maGV = input[0];
            StringBuilder tenGV = new StringBuilder();
            for (int i = 1; i < input.length; i++) {
                tenGV.append(input[i]);
                tenGV.append(" ");
            }
            dsgv.add(new GiangVien(maGV, tenGV.toString().trim()));
        }

        t = Integer.parseInt(sc.nextLine());
        ArrayList<String> ds = new ArrayList<>();
        while (t-- > 0)
            ds.add(sc.nextLine());
        String gv = sc.nextLine();
        for (GiangVien x : dsgv) {
            if (x.getMaGV().equals(gv)) {
                System.out.println("Giang vien: " + x.getTen());
                float m = 0;
                for (String s : ds) {
                    String[] input = s.split("\\s+");
                    float soGio = Float.parseFloat(input[2]);
                    if (x.getMaGV().equals(input[0])) {
                        for (Lop lop : dsl) {
                            if (lop.getMaLop().equals(input[1])) {
                                System.out.print(lop.getTen() + " ");
                                System.out.println(soGio);
                            }
                        }
                        m = m + soGio;
                    }
                }
                System.out.printf("Tong: %.2f\n", m);
            }
        }
    }
}
