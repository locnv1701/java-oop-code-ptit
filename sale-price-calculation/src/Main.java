import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        ArrayList<Product> productArrayList = new ArrayList<>();
        while (t-- > 0) {
            Product product = new Product(sc.nextLine(), sc.nextLine(), Long.parseLong(sc.nextLine()), Long.parseLong(sc.nextLine()));
            productArrayList.add(product);
        }

        productArrayList.sort(null);
        for (Product product : productArrayList) {
            System.out.println(product);
        }
    }
}

