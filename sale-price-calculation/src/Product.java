public class Product implements Comparable<Product> {
    public static int count = 0;
    private String id;
    private String name;
    private String unit;
    private long cost;
    private long number;
    private long transportFee;
    private long money;
    private long price;


    public Product(String name, String unit, long cost, long number) {
        count++;
        this.id = "MH" + String.format("%02d", count);
        this.name = name;
        this.unit = unit;
        this.cost = cost;
        this.number = number;
        this.transportFee = (long) Math.ceil(cost * number * 5 / 100.0);
        this.money = cost * number + transportFee;
        this.price = (long) (Math.ceil(money * 102.0 / 100 / number /100)*100);
    }

    @Override
    public String toString() {
        return id + ' ' + name + ' ' + unit + " " + transportFee + " " + money + " " + price;
    }

    @Override
    public int compareTo(Product o) {
        Long a = o.price;
        Long b = price;
        return a.compareTo(b);
    }
}
