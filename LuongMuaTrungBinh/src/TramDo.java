import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TramDo {
    private String ten;
    private Date start;
    private Date finish;
    private int luongMua;
    private long time;

    public TramDo(String ten, String start, String finish, int luongMua) throws ParseException {
        this.ten = ten;
        this.start = new SimpleDateFormat("HH:mm").parse(start);
        this.finish = new SimpleDateFormat("HH:mm").parse(finish);
        this.luongMua = luongMua;
        this.time = (this.finish.getTime() - this.start.getTime()) / 60000;
    }

    public String getTen() {
        return ten;
    }

    public Date getStart() {
        return start;
    }

    public Date getFinish() {
        return finish;
    }

    public int getLuongMua() {
        return luongMua;
    }

    public long getTime() {
        return time;
    }

    @Override
    public String toString() {
        return "TramDo{" +
                "ten='" + ten + '\'' +
                ", luongMua=" + luongMua +
                ", time=" + time +
                '}';
    }
}
