import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws ParseException {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        ArrayList<TramDo> dstd = new ArrayList<>();
        ArrayList<String> tenTramDoList = new ArrayList<>();
        while (t-- > 0) {
            String tenTramDo = sc.nextLine();
            if (!tenTramDoList.contains(tenTramDo)) {
                tenTramDoList.add(tenTramDo);
            }
            dstd.add(new TramDo(tenTramDo, sc.nextLine(), sc.nextLine(), Integer.parseInt(sc.nextLine())));
        }
        for (int i = 0; i < tenTramDoList.size(); i++) {
            int finalI = i;
            System.out.printf("T%02d %s", i + 1, tenTramDoList.get(finalI));
            int luongMua = 0;
            int thoiGian = 0;
            for (int j = 0; j < dstd.size(); j++) {
                if (dstd.get(j).getTen().contains(tenTramDoList.get(finalI))) {
                    luongMua += dstd.get(j).getLuongMua();
                    thoiGian += dstd.get(j).getTime();
                }
            }
            double TB = luongMua * 60.0 / thoiGian;
            System.out.printf(" %.2f\n", TB);
        }
    }
}

