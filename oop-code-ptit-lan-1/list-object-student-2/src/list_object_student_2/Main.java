package list_object_student_2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String tmp = sc.nextLine();
        Student[] listOfStudent = new Student[n];
        for (int i = 0; i < n; i++) {
            String name = sc.nextLine();
            String grade = sc.nextLine();
            String dateOfBirth = sc.nextLine();
            float GPA = sc.nextFloat();
            String tmp1 = sc.nextLine();
            listOfStudent[i] = new Student(name,grade,dateOfBirth,GPA);
        }
        for (int i = 0; i < n; i++) {
            listOfStudent[i].display();
        }

    }
}
