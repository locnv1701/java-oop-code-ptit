package khai_bao_lop_hinh_chu_nhat;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double width = sc.nextDouble();
        double height = sc.nextDouble();
        String color = sc.next();
        String res = "";
        color = color.toLowerCase();
        res += String.valueOf(color.charAt(0)).toUpperCase() + color.substring(1,color.length());
        if (width <= 0 || height <= 0) {
            System.out.println("INVALID");
        } else if (width != (int) width || height != (int) height) {
            System.out.println("INVALID");
        } else {
            Rectangle rectangle = new Rectangle(width, height, res );
            System.out.print((int) (rectangle.findPerimeter()) + " ");
            System.out.print((int) (rectangle.findArea()) + " ");
            System.out.print(rectangle.getColor());
        }
    }
}
