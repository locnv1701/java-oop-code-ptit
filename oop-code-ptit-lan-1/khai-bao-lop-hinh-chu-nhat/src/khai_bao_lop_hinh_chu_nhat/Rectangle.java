package khai_bao_lop_hinh_chu_nhat;

public class Rectangle {
    private double width;
    private double height;
    private static String color;

    public Rectangle() {
        this.width = 1;
        this.height = 1;
    }

    public Rectangle(double width, double height, String color) {
        this.width = width;
        this.height = height;
        this.color = color;
    }

    public double getWidth() {
        return width;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public static String getColor() {
        return color;
    }
    public static void setColor(String color) {
        Rectangle.color = color;
    }

    public double findArea() {
        return height * width;
    }

    public double findPerimeter() {
        return (height + width) * 2;
    }
}
