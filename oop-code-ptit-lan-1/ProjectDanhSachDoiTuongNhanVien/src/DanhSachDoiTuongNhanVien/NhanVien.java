package DanhSachDoiTuongNhanVien;

import java.util.Scanner;

import static java.lang.Character.isDigit;

public class NhanVien {
    private int iD;
    private static int count = 0;
    private String name;
    private String sex;
    private String dateOfBirth;
    private String address;
    private long taxCode;
    private String contractSigningDate;

    public NhanVien(String name, String sex, String dateOfBirth, String address, long taxCode, String contractSigningDate) {
        count += 1;
        this.iD = count;
        this.name = name;
        this.sex = sex;
        this.dateOfBirth = dateOfBirth;
        this.address = address;
        this.taxCode = taxCode;
        this.contractSigningDate = contractSigningDate;
    }

    public String formatDate(String date) {
        if (isDigit(date.charAt(2))) {
            date = "0" + date;
        }
        if (isDigit(date.charAt(5))) {
            date = date.substring(0, 3) + "0" + date.substring(3, date.length());
        }
        return date;
    }

    public void display() {
        dateOfBirth = formatDate(dateOfBirth);
        contractSigningDate = formatDate(contractSigningDate);
        if (iD < 10) {
            System.out.println("0000" + iD + " " + name + " " + sex + " " + dateOfBirth + " " + address + " " + taxCode + " " + contractSigningDate);

        } else {
            System.out.println("000" + iD + " " + name + " " + sex + " " + dateOfBirth + " " + address + " " + taxCode + " " + contractSigningDate);
        }
    }
}
