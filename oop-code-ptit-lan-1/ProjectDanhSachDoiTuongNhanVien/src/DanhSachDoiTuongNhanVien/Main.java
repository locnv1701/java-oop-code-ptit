package DanhSachDoiTuongNhanVien;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        NhanVien[] nv = new NhanVien[n];
        for (int i = 0; i < n; i++) {
            String tmp = sc.nextLine();
            String name = sc.nextLine();
            String sex = sc.nextLine();
            String dateOfBirth = sc.nextLine();
            String address = sc.nextLine();
            long taxCode = sc.nextLong();
            String tmp1 = sc.nextLine();
            String contractSigningDate = sc.next();
            nv[i] = new NhanVien(name, sex, dateOfBirth, address, taxCode, contractSigningDate);
        }
        for (int i = 0; i < n; i++) {
            nv[i].display();
        }
    }
}
