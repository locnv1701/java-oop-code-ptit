package SapXepDanhSachDoiTuongNhanVien;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws ParseException {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String tmp = sc.nextLine();
        List<NhanVien> ans = new ArrayList<>();
        for (int i = 0; i < n; i++) {

            String name = sc.nextLine();
            String sex = sc.nextLine();
            String dateOfBirth = sc.nextLine();
            String address = sc.nextLine();
            String taxCode = sc.nextLine();
            String contractSigningDate = sc.nextLine();
            NhanVien nhanVien = new NhanVien(name, sex, dateOfBirth, address, taxCode, contractSigningDate);
            ans.add(nhanVien);
        }
        ans.sort(null);
        for(NhanVien ele : ans){
            ele.display();
        }
    }
}
