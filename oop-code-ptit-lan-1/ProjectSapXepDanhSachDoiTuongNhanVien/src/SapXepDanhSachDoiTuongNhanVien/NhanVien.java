package SapXepDanhSachDoiTuongNhanVien;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NhanVien implements Comparable<NhanVien> {
    private int iD;
    private static int count = 0;
    private String name;
    private String sex;
    private Date dateOfBirth;
    private String address;
    private String taxCode;
    private String contractSigningDate;

    public NhanVien(String name, String sex, String dateOfBirth, String address, String taxCode, String contractSigningDate) throws ParseException {
        count += 1;
        this.iD = count;
        this.name = name;
        this.sex = sex;
        Date temp = new SimpleDateFormat("dd/MM/yyyy").parse(dateOfBirth);
        this.dateOfBirth = temp;
        this.address = address;
        this.taxCode = taxCode;
        this.contractSigningDate = contractSigningDate;
    }

    public void display() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String date = formatter.format(this.dateOfBirth);
        if (iD < 10) {
            System.out.println("0000" + iD + " " + name + " " + sex + " " + date + " " + address + " " + taxCode + " " + contractSigningDate);

        } else {
            System.out.println("000" + iD + " " + name + " " + sex + " " + date + " " + address + " " + taxCode + " " + contractSigningDate);
        }
    }

    @Override
    public int compareTo(NhanVien o) {
        return this.dateOfBirth.compareTo(o.dateOfBirth);
    }
}
