package hotel_bill;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Bill implements Comparable<Bill> {
    private String iD;
    private String name;
    private int room;
    private Date start;
    private Date end;
    private int tip;
    private int day;

    public int countDays(String start, String end) {
        Date d1 = null;
        Date d2 = null;
        try {
            d1 = new SimpleDateFormat("dd/MM/yyyy").parse(start);
            d2 = new SimpleDateFormat("dd/MM/yyyy").parse(end);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return (int) ((d2.getTime() - d1.getTime()) / (1000 * 24 * 60 * 60)) + 1;
    }

    public Bill(String name, int room, String start, String end, int tip, int count) throws ParseException {
        if (count < 10) {
            this.iD = "KH0" + count;
        } else {
            this.iD = "KH" + count;
        }
        this.name = name;
        this.room = room;
        this.tip = tip;
        this.day = countDays(start, end);

    }

    public int toMoney() {
        int money = 0;
        int flood = this.room / 100;
        int price = 0;
        if (flood == 1) {
            price = 25;
        }
        if (flood == 2) {
            price = 34;
        }
        if (flood == 3) {
            price = 50;
        }
        if (flood == 4) {
            price = 80;
        }
        return (int) (this.day * price + this.tip);
    }

    public void display() {
        System.out.println(iD + " " + name + " " + room + " " + day + " " + toMoney());
    }

    @Override
    public int compareTo(Bill o) {
        Integer a = o.toMoney();
        Integer b = this.toMoney();
        return a.compareTo(b);
    }
}
