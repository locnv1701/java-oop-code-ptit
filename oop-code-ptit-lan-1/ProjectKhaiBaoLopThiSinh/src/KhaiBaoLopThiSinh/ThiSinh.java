package KhaiBaoLopThiSinh;
import java.math.BigDecimal;
import java.util.Scanner;
public class ThiSinh {
    private String name;
    private String dateOfBirth;
    private float mark1;
    private float mark2;
    private float mark3;
    private float GPA;

    public ThiSinh(){

    }

    public void nhap(){
        Scanner sc = new Scanner(System.in);
        this.name = sc.nextLine();
        this.dateOfBirth = sc.next();
        this.mark1 = sc.nextFloat();
        this.mark2 = sc.nextFloat();
        this.mark3 = sc.nextFloat();
        this.GPA = mark1 + mark2 + mark3;
    }

    public static BigDecimal round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd;
    }

    public void display(){
        System.out.print(name + " " + dateOfBirth + " ");
        BigDecimal result = round(GPA,1);
        System.out.println(result);
    }


}

