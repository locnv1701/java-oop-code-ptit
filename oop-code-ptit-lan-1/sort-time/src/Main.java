import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        ArrayList<Time> listTime = new ArrayList<>();
        while (n-->0) {
            Time tmp = new Time(sc.nextInt(), sc.nextInt(), sc.nextInt());
            listTime.add(tmp);
        }
        listTime.sort(null);
        listTime.forEach(System.out::println);
    }
}
