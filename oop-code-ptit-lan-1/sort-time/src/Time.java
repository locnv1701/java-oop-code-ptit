
public class Time implements Comparable<Time> {
    private Integer hour;
    private Integer minute;
    private Integer second;

    public Time(Integer hour, Integer minute, Integer second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    @Override
    public String toString() {
        return hour + " " + minute + " " + second;

    }

    @Override
    public int compareTo(Time o) {
        if (hour.compareTo(o.hour) != 0)
            return hour.compareTo(o.hour);
        else if (minute.compareTo(o.minute) != 0)
            return minute.compareTo(o.minute);
        else return second.compareTo(o.second);
    }
}
