package perimeter_triangle;

import java.util.*;
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while(t-->0){
            Point p1 = new Point(sc.nextDouble(),sc.nextDouble());
            Point p2 = new Point(sc.nextDouble(),sc.nextDouble());
            Point p3 = new Point(sc.nextDouble(),sc.nextDouble());
            double l1 = p1.distance(p2);
            double l2 = p1.distance(p3);
            double l3 = p2.distance(p3);
            if((l1+l2 <= l3) || (l1+l3 <= l2) || (l2+l3 <= l1)){
                System.out.println("INVALID");
            }
            else{
                System.out.printf("%.3f",l1+l2+l3);
                System.out.println();
            }
        }
    }
}