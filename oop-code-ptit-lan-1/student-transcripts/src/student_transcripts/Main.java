package student_transcripts;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        List<Student> studentList = new ArrayList<>();
        float[] mark = new float[10];
        for (int i = 0; i < n; i++) {
            String tmp = sc.nextLine();
            String name = sc.nextLine();
            for (int j = 0; j < 10; j++) {
                mark[j] = sc.nextFloat();
            }
            Student student = new Student(name, mark, i + 1);
            studentList.add(student);
        }
        studentList.sort(null);
        for (Student student : studentList) {
            student.display();
        }

    }
}