package student_transcripts;

public class Student implements Comparable<Student> {
    private String iD;
    private String name;
    private float mark1;
    private float mark2;
    private float mark3;
    private float mark4;
    private float mark5;
    private float mark6;
    private float mark7;
    private float mark8;
    private float mark9;
    private float mark10;


    public Student() {

    }

    public Student(String name, float[] listMark, int count) {
        this.iD = count < 10 ? "HS0" + count : "HS" + count;
        this.name = name;
        mark1 = listMark[0];
        mark2 = listMark[1];
        mark3 = listMark[2];
        mark4 = listMark[3];
        mark5 = listMark[4];
        mark6 = listMark[5];
        mark7 = listMark[6];
        mark8 = listMark[7];
        mark9 = listMark[8];
        mark10 = listMark[9];

    }

    public float getGPA() {
        float GPA = mark1 * 2 + mark2 * 2 + mark3 + mark4 + mark5 + mark6 + mark7 + mark8 + mark9 + mark10;
        return GPA / 12;
    }

    public void display() {
        System.out.print(iD + " " + name);
        double gpa = getGPA();
        if (gpa >= Math.ceil(gpa * 100) / 100 + 0.05) {
            gpa = Math.round(gpa * 100) / 100;
        } else {
            gpa = Math.ceil(gpa * 100) / 100;
        }
        System.out.printf(" %.1f ", gpa);
        String ranking = "";
        if (getGPA() >= 9f) {
            ranking = "XUAT SAC";
        } else if (getGPA() >= 8f) {
            ranking = "GIOI";
        } else if (getGPA() >= 7f) {
            ranking = "KHA";
        } else if (getGPA() >= 5f) {
            ranking = "TB";
        } else
            ranking = "YEU";
        System.out.println(ranking);
    }

    @Override
    public int compareTo(Student o) {
        Float a = o.getGPA();
        Float b = this.getGPA();
        if (a.compareTo(b) != 0)
            return a.compareTo(b);
        else return this.iD.compareTo(o.iD);
    }
}
