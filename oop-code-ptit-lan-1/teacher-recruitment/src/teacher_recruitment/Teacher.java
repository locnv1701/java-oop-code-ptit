package teacher_recruitment;

import java.security.Principal;
import java.sql.Array;
import java.util.ArrayList;

public class Teacher implements Comparable<Teacher> {
    private static int count = 0;
    private String iD;
    private String name;
    private String code;
    private double mark2;
    private double mark1;
    private String subject;
    private double plusPoint;

    public Teacher() {

    }

    public Teacher(String name, String code, double mark2, double mark1) {
        count += 1;
        if (count < 10) {
            this.iD = "GV0" + count;
        } else {
            this.iD = "GV" + count;
        }
        this.name = name;
        this.code = code;
        this.mark1 = mark1;
        this.mark2 = mark2;
        if (code.charAt(0) == 'A') {
            this.subject = "TOAN";
        }
        if (code.charAt(0) == 'B') {
            this.subject = "LY";
        }
        if (code.charAt(0) == 'C') {
            this.subject = "HOA";
        }
        if (code.charAt(1) == '1') {
            this.plusPoint = 2.0;
        }
        if (code.charAt(1) == '2') {
            this.plusPoint = 1.5;
        }
        if (code.charAt(1) == '3') {
            this.plusPoint = 1.0;
        }
        if (code.charAt(1) == '4') {
            this.plusPoint = 0.0;
        }

    }

    public double totalScore() {
        return this.mark2 * 2 + this.mark1 + this.plusPoint;
    }

    public void display() {
        String result = "";
        if (totalScore() > 18) {
            result = "TRUNG TUYEN";
        } else {
            result = "LOAI";
        }
        System.out.print(iD + " " + name + " " + subject);
        System.out.printf(" %.1f ", totalScore());
        System.out.println(result);


    }

    @Override
    public int compareTo(Teacher o) {
        return String.valueOf(o.totalScore()).compareTo(String.valueOf(this.totalScore()));
    }
}
