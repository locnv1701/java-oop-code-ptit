package bill;

public class Bill implements Comparable<Bill>{
    private String iD;
    private String name;
    private int quantity;
    private long price;
    private long discount;

    public Bill(){
    }
    public Bill(String iD, String name, int quantity, long price, long discount){
        this.iD = iD;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.discount = discount;
    }
    public long priceTotal(){
        return price*quantity-discount;
    }

    public void display(){
        System.out.println(iD + " " + name + " " + quantity + " " + price + " " + discount + " " + priceTotal() );
    }
    @Override
    public int compareTo(Bill o) {
        Long a = this.priceTotal();
        Long b = o.priceTotal();
        return b.compareTo(a);
    }
}

