package bill;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        List<Bill> billList = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            String tmp = sc.nextLine();
            String iD = sc.nextLine();
            String name = sc.nextLine();
            int quantity = sc.nextInt();
            long price = sc.nextLong();
            long discount = sc.nextLong();
            Bill bill = new Bill(iD, name, quantity, price, discount);
            billList.add(bill);
        }
        billList.sort(null);
        for (Bill bill : billList) {
            bill.display();
        }
    }

}
