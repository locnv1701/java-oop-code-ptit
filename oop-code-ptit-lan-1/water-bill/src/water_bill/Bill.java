package water_bill;

public class Bill implements Comparable<Bill> {
    private String iD;
    private String name;
    private int start;
    private int end;
    private int numberWater;

    public Bill() {

    }

    public Bill(String name, int start, int end, int count) {
        if (count < 10) {
            this.iD = "KH0" + count;
        } else {
            this.iD = "KH" + count;
        }
        this.name = name;
        this.numberWater = end - start;
    }

    public int toMoney() {
        int money = 0;
        for (int i = 1; i <= numberWater; i++) {
            if (i <= 50) {
                money += 100;
            } else if (i <= 100) {
                money += 150;
            } else {
                money += 200;
            }
        }
        int vat = 2;
        if (numberWater <= 50) {
            vat = 2;
        } else if (numberWater <= 100) {
            vat = 3;
        } else {
            vat = 5;
        }
        vat += 100;

        if (money % 100 == 0) {
            return money* vat / 100 ;
        } else {
            return money* vat/ 100  + 1;
        }
    }

    void display() {
        System.out.println(iD + " " + name + " " + toMoney());
    }

    @Override
    public int compareTo(Bill o) {
        Integer a = o.toMoney();
        Integer b = this.toMoney();
        return a.compareTo(b);
    }
}
