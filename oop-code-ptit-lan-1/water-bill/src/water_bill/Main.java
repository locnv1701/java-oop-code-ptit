package water_bill;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        List<Bill> listBill = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            String tmp = sc.nextLine();
            String name = sc.nextLine();
            int start = sc.nextInt();
            int end = sc.nextInt();
            Bill bill = new Bill(name,start,end, i+1);
            listBill.add(bill);
        }

        listBill.sort(null);
        for (Bill bill : listBill) {
            bill.display();
        }
    }
}

