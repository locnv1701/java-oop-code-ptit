package recruitment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        List<Candidates> listCandidates = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            String tmp = sc.nextLine();
            String name = sc.nextLine();
            float mark1 = sc.nextFloat();
            float mark2 = sc.nextFloat();
            Candidates candidates = new Candidates(name, mark1, mark2);
            listCandidates.add(candidates);
        }

        listCandidates.sort(null);
        for (Candidates candidates : listCandidates) {
            candidates.display();
        }

    }
}