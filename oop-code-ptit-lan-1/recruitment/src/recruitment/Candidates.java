package recruitment;

public class Candidates implements Comparable<Candidates> {
    private static int count = 0;
    private String iD;
    private String name;
    private float mark1;
    private float mark2;


    public Candidates() {

    }

    public float formatMark(float mark) {
        if (mark > 10) {
            return mark / 10;
        }
        else return mark;
    }

    public Candidates(String name, float mark1, float mark2) {
        count += 1;
        if (count < 10) {
            this.iD = "TS0" + count;
        } else {
            this.iD = "TS" + count;
        }
        this.name = name;
        this.mark1 = formatMark(mark1);
        this.mark2 = formatMark(mark2);

    }

    public float totalScore() {
        return (this.mark2 + this.mark1) / 2f;
    }

    public void display() {
        String result = "";
        if (totalScore() > 9.5f) {
            result = "XUAT SAC";
        } else if (totalScore() >= 8f) {
            result = "DAT";

        } else if (totalScore() >= 5f) {
            result = "CAN NHAC";
        } else {
            result = "TRUOT";
        }
        System.out.print(iD + " " + name);
        System.out.printf(" %.2f ", totalScore());
        System.out.println(result);
    }

    @Override
    public int compareTo(Candidates o) {
        Float a=this.totalScore();
        Float b=o.totalScore();
        return b.compareTo(a);
    }
}
