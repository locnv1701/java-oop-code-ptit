package list_object_student_3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String tmp = sc.nextLine();
        StudentWrapper[] listOfStudent = new StudentWrapper[n];
        for (int i = 0; i < n; i++) {
            String name = sc.nextLine();
            String grade = sc.nextLine();
            String dateOfBirth = sc.nextLine();
            float GPA = sc.nextFloat();
            String tmp1 = sc.nextLine();
            Student studentTmp = new Student(name,grade,dateOfBirth,GPA);
            listOfStudent[i] = new StudentWrapper(studentTmp);
        }

        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if(!Student.compare(listOfStudent[j].student, listOfStudent[j+1].student)) {
                    Student.swap(listOfStudent[j], listOfStudent[j+1]);
                }
            }
        }

        for (int i = 0; i < n; i++) {
            listOfStudent[i].student.display();
        }

    }
}
