package list_object_student_3;

import java.util.Locale;

import static java.lang.Character.isDigit;

public class Student {
    private static int count = 0;
    private String name;
    private String iD;
    private String grade;
    private String dateOfBirth;
    private float GPA;

    public Student() {
        this.iD = "";
        this.name = "";
        this.grade = "";
        this.dateOfBirth = "";
        this.GPA = 0;
    }

    public Student(String name, String grade, String dateOfBirth, float GPA) {
        count++;
        if (count < 10) {
            this.iD = "B20DCCN00" + count;
        } else {
            this.iD = "B20DCCN0" + count;
        }
        this.name = name;
        this.grade = grade;
        this.dateOfBirth = dateOfBirth;
        this.GPA = GPA;
    }

    public void formatDate() {
        if (isDigit(dateOfBirth.charAt(2))) {
            dateOfBirth = "0" + dateOfBirth;
        }
        if (isDigit(dateOfBirth.charAt(5))) {
            dateOfBirth = dateOfBirth.substring(0, 3) + "0" + dateOfBirth.substring(3, dateOfBirth.length());
        }
    }

    public void formatName(){
        String[] token = name.trim().toLowerCase().replaceAll("\\s+", " ").split(" ");
        String tmp = "";
        for (int i = 0; i < token.length; i++) {
            tmp += String.valueOf(token[i].charAt(0)).toUpperCase() + token[i].substring(1,token[i].length());
            tmp += " ";
        }
        name = tmp;
    }
    public static boolean compare(Student student1, Student student2) {
        if(student1.GPA > student2.GPA)
            return true;
        return false;
    }

    public static void swap(StudentWrapper studentWrapper1, StudentWrapper studentWrapper2) {
        Student tmp = studentWrapper1.student;
        studentWrapper1.student = studentWrapper2.student;
        studentWrapper2.student = tmp;
    }

    public void display() {
        formatDate();
        formatName();
        System.out.print(iD + " " + name + "" + grade + " " + dateOfBirth + " ");
        System.out.printf("%.2f", this.GPA);
        System.out.println();
    }

}
