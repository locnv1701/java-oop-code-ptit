package KhaiBaoLopNhanVien;

import java.util.Scanner;

import static java.lang.Character.isDigit;

public class NhanVien {
    private String iD = "00001";
    private String name;
    private String sex;
    private String dateOfBirth;
    private String address;
    private long taxCode;
    private String contractSigningDate;

    public NhanVien() {
    }


    public void input() {
        Scanner sc = new Scanner(System.in);
        this.name = sc.nextLine();
        this.sex = sc.nextLine();
        this.dateOfBirth = sc.nextLine();
        this.address = sc.nextLine();
        this.taxCode = sc.nextLong();
        String tmp = sc.nextLine();
        this.contractSigningDate = sc.nextLine();
    }

    public String formatDate(String date) {
        if (isDigit(date.charAt(2))) {
            date = "0" + date;
        }
        if (isDigit(date.charAt(5))) {
            date = date.substring(0, 3) + "0" + date.substring(3, date.length());
        }
        return date;
    }

    public void display() {
        dateOfBirth = formatDate(dateOfBirth);
        contractSigningDate = formatDate(contractSigningDate);
        System.out.println(iD + " " + name + " " + sex + " " + dateOfBirth + " " + address + " " + taxCode + " " + contractSigningDate);
    }
}
