package KhaiBaoLopSinhVien;

import java.math.BigDecimal;
import java.util.Scanner;
import static java.lang.Character.isDigit;

public class SinhVien {
    private String name;
    private String iD;
    private String grade;
    private String dateOfBirth;
    private float GPA;

    public SinhVien() {
        this.iD = "";
        this.name = "";
        this.grade = "";
        this.dateOfBirth = "";
        this.GPA = 0;
    }

    public void input() {
        Scanner sc = new Scanner(System.in);
        this.iD = "B20DCCN001";
        this.name = sc.nextLine();
        this.grade = sc.next();
        this.dateOfBirth = sc.next();
        this.GPA = sc.nextFloat();
    }

    public static BigDecimal round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd;
    }

    public void display() {
        if(isDigit(dateOfBirth.charAt(2))){
            dateOfBirth = "0" + dateOfBirth;
        }
        if(isDigit(dateOfBirth.charAt(5))){
            dateOfBirth = dateOfBirth.substring(0,3) + "0" + dateOfBirth.substring(3, dateOfBirth.length());
        }
        System.out.print(iD + " Nguyen Van A " + grade + " " + dateOfBirth + " ");
        BigDecimal result = round(GPA, 2);
        System.out.println(result);
    }
}

