package TongPhanSo;

import java.util.Scanner;

public class PhanSo {
    private long tuSo;
    private long mauSo;

    public PhanSo() {
    }

    public PhanSo(long tuSo, long mauSo) {
        this.tuSo = tuSo;
        this.mauSo = mauSo;
    }

    public void nhap() {
        Scanner sc = new Scanner(System.in);
        this.tuSo = sc.nextLong();
        this.mauSo = sc.nextLong();
    }

    public static long gcd(long a, long b) {
        if (b == 0)
            return a;
        return gcd(b, a % b);
    }

    public static long lcm(long a, long b) {
        return a * b / gcd(a, b);
    }

    public void rutGon() {
        long gcd = gcd(tuSo, mauSo);
        tuSo = tuSo / gcd;
        mauSo = mauSo / gcd;
    }

//    public static PhanSo tongPhanSo(PhanSo p, PhanSo q) {
//        PhanSo s = new PhanSo();
//        s.tuSo = p.tuSo * lcm(p.mauSo, q.mauSo) / p.mauSo + q.tuSo * lcm(p.mauSo, q.mauSo) / q.mauSo;
//        s.mauSo = lcm(p.mauSo, q.mauSo);
//        return s;
//    }

    public PhanSo tongPhanSo(PhanSo q) {
        PhanSo s = new PhanSo();
        s.tuSo = this.tuSo * lcm(this.mauSo, q.mauSo) / this.mauSo + q.tuSo * lcm(this.mauSo, q.mauSo) / q.mauSo;
        s.mauSo = lcm(this.mauSo, q.mauSo);
        return s;
    }

    public String toString() {
        return tuSo + "/" + mauSo;
    }

    public void display() {
        System.out.println(tuSo + "/" + mauSo);
    }
}
