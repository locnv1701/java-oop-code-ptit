package TongPhanSo;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Long a = input.nextLong(), b = input.nextLong(), c = input.nextLong(), d = input.nextLong();
        Long e = gcd(a, b), f = gcd(c, d);
        a /= e;
        b /= e;
        c /= f;
        d /= f;
        e = gcd(b, d);
        e = (b * d) / e;
        f = e / d;
        e = e / b;
        a *= e;
        c *= f;
        f = a + c;
        e *= b;
        a = gcd(e, f);
        e /= a;
        f /= a;
        System.out.println(f + "/" + e);

    }
    static Long gcd(Long a, Long b) {
        return b == 0 ? a : gcd(b, a % b);
    }
}

