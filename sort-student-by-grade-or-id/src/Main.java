import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<Student> studentArrayList = new ArrayList<>();
        while(sc.hasNext()){
            Student student = new Student(sc.nextLine(), sc.nextLine(), sc.nextLine(), sc.nextLine());
            studentArrayList.add(student);
        }
        studentArrayList.sort(null);
        studentArrayList.forEach(System.out::println);

    }
}
