import java.util.HashMap;
import java.util.Map;
public class Product implements Comparable<Product> {
    private String name;
    private int number;
    private int price;
    private String id;
    public static Map<String, Integer> map = new HashMap<>();


    public Product(String name, int number, int price) {
        this.name = name;
        this.number = number;
        this.price = price;
        String id = "";
        String[] word = name.split(" ");
//        System.out.println(Arrays.toString(word));
        id += Character.toUpperCase(word[0].charAt(0));
        id += Character.toUpperCase(word[1].charAt(0));
        if (map.containsKey(id)) {
            map.put(id, map.get(id) + 1);
        } else {
            map.put(id, 1);
        }
        this.id = id + String.format("%02d", map.get(id));
    }

    public Integer getDiscount() {
        int discount = 0;
        if (number > 10) {
            discount = 5;
        } else if (number >= 8) {
            discount = 2;
        } else if (number >= 5) {
            discount = 1;
        } else discount = 0;
        return price * number * discount / 100;
    }

    public void display() {
        System.out.println(id + " " + name + " " + getDiscount() +
                " " + (price * number - getDiscount()));

    }

    @Override
    public int compareTo(Product o) {
        return o.getDiscount().compareTo(getDiscount());
    }
}