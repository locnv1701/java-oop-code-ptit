import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        int count = 0;
        ArrayList<Fee> feeArrayList = new ArrayList<>();
        while (t-- > 0) {
            count++;
            Fee fee = new Fee(count, sc.nextLine(), Integer.parseInt(sc.nextLine()), Integer.parseInt(sc.nextLine()));
            feeArrayList.add(fee);
        }

        feeArrayList.sort(null);
        for (Fee fee : feeArrayList) {
            System.out.println(fee);
        }
    }
}
