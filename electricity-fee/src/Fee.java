public class Fee implements Comparable<Fee> {
    private int count;
    private String id;
    private long start;
    private long ending;
    private long money;
    private long coefficient;
    private long additional;

    public Fee(int count, String id, int start, int ending) {
        this.id = id;
        this.count = count;
        this.start = start;
        this.ending = ending;
        coefficient = 0;
        if (id.equals("KD"))
            coefficient = 3L;
        if (id.equals("NN"))
            coefficient = 5L;
        if (id.equals("TT"))
            coefficient = 4L;
        if (id.equals("CN"))
            coefficient = 2L;
        money = (ending - start) * coefficient * 550;
        additional = 0L;
        if (ending - start > 100)
            additional = money;
        else if (ending - start >= 50)
            additional =  (long) (Math.round(money * 35 / 100.0));
    }

    public String getId() {
        return id;
    }

    public Long toMoney() {
        return money + additional;
    }

    @Override
    public String toString() {
        return String.format("KH%02d", count)+" " + coefficient + " " + money + " " + additional + " " +( money + additional);
    }

    @Override
    public int compareTo(Fee o) {
        return o.toMoney().compareTo(toMoney());
    }
}
