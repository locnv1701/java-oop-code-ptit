import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        int p = m;
        String tmp = sc.nextLine();
        ArrayList<SinhVien> dssv = new ArrayList<>();
        ArrayList<Nhom> dsn = new ArrayList<>();
        int count = 0;
        while (m--> 0) {
            count++;
            Nhom nhom = new Nhom(count);
            dsn.add(nhom);
        }

        while (n-- > 0) {
            String maSV = sc.nextLine();
            String ten = sc.nextLine();
            String sdt = sc.nextLine();
            int maNhom = Integer.parseInt(sc.nextLine());

            Nhom nhom = null;
            for (int i = 0; i < dsn.size(); i++) {
                if (dsn.get(i).getMaNhom() == maNhom) {
                    nhom = dsn.get(i);
                }
            }
            SinhVien sinhVien = new SinhVien(maSV, ten, sdt, nhom);
            dssv.add(sinhVien);
        }

        for (int i = 0; i < p; i++) {
            dsn.get(i).setTenNhom(sc.nextLine());
        }

        dssv.sort(null);
        dssv.forEach(System.out::println);
    }
}



























