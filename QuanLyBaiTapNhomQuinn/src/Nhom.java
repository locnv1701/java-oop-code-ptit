public class Nhom {
    private int maNhom;
    private String tenNhom;

    public Nhom(int maNhom) {
        this.maNhom = maNhom;
    }

    public int getMaNhom() {
        return maNhom;
    }

    public String getTenNhom() {
        return tenNhom;
    }

    public void setTenNhom(String tenNhom) {
        this.tenNhom = tenNhom;
    }
}
