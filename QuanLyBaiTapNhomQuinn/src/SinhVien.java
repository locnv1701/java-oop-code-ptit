public class SinhVien implements Comparable<SinhVien> {
    private String maSV;
    private String ten;
    private String std;
    private Nhom nhom;

    public SinhVien(String maSV, String ten, String std, Nhom nhom) {
        this.maSV = maSV;
        this.ten = ten;
        this.std = std;
        this.nhom = nhom;
    }

    public Nhom getNhom() {
        return nhom;
    }

    @Override
    public String toString() {
        return maSV + " " + ten + " " + std + " " + nhom.getMaNhom() + " "+ nhom.getTenNhom() ;
    }

    @Override
    public int compareTo(SinhVien o) {
        return maSV.compareTo(o.maSV);
    }
}
