import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class KhachHang implements Comparable<KhachHang> {
    private String ma;
    private String ten;
    private String gioiTinh;
    private Date ngaySinh;
    private String diaChi;

    public KhachHang(int ma, String ten, String gioiTinh,
                     String ngaySinh, String diaChi) throws ParseException {
        this.ma = String.format("KH%03d", ma);
        this.ten = chuanHoaTen(ten);
        this.gioiTinh = gioiTinh;
        this.ngaySinh = new SimpleDateFormat("dd/MM/yyyy").parse(ngaySinh);
        this.diaChi = diaChi;
    }

    private String chuanHoaTen(String str) {
        str = str.toLowerCase();
        str = str.trim();
        str = str.replaceAll("\\s+", " ");
        String[] token = str.split(" ");
        String res = "";
        for (int i = 0; i < token.length; i++) {
            res += String.valueOf(token[i].charAt(0)).toUpperCase() + token[i].substring(1, token[i].length());
            res += " ";
        }
        return res.trim();
    }

    @Override
    public String toString() {
        return ma + ' ' + ten + ' ' + gioiTinh+ " " + diaChi + " " + new SimpleDateFormat("dd/MM/yyyy").format(ngaySinh);
    }


    @Override
    public int compareTo(KhachHang o) {
        return -o.ngaySinh.compareTo(ngaySinh);
    }
}
