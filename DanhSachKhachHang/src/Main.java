import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws ParseException {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        ArrayList<KhachHang> dskh = new ArrayList<>();
        int count = 0;
        while (t-- > 0) {
            count++;
            dskh.add(new KhachHang(count, sc.nextLine(), sc.nextLine(), sc.nextLine(), sc.nextLine()));
        }
        dskh.sort(null);
        dskh.forEach(System.out::println);
    }
}
