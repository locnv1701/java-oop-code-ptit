public class Bill implements Comparable<Bill>{
    private String id;
    private String name;
    private long price;
    private long number;
    private long discount;

    public Bill(String name, String id, long price, long number) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.number = number;
        if (id.charAt(4) == '1') {
            this.discount = price * number / 2;
        } else {
            this.discount = price * number * 3 / 10;
        }
    }

    @Override
    public String toString() {
        return name + ' ' + id + ' ' + id.substring(1, 4) + ' ' + discount + ' ' + (number * price - discount);
    }

    @Override
    public int compareTo(Bill o) {
        return id.substring(1,4).compareTo(o.id.substring(1,4));
    }
}
