import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        ArrayList<Bill> billArrayList   = new ArrayList<>();
        while(t-->0){
            Bill bill = new Bill(sc.nextLine(), sc.nextLine(), Integer.parseInt(sc.nextLine()), Integer.parseInt(sc.nextLine()));
            billArrayList.add(bill);
        }
        billArrayList.sort(null);
        billArrayList.forEach(System.out::println);
    }
}
