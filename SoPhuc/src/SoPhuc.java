public class SoPhuc {
    private int f;
    private int s;

    public SoPhuc(int f, int s) {
        this.f = f;
        this.s = s;
    }

    public SoPhuc phepTinhC(SoPhuc b) {
        int fi = f * f - s * s + f * b.f - s * b.s;
        int se = 2 * f * s + f * b.s + s * b.f;
        return new SoPhuc(fi, se);
    }

    public SoPhuc phepTinhD(SoPhuc b) {
        int fi = (f + b.f) * (f + b.f) - (s + b.s) * (s + b.s);
        int se = 2 * (f + b.f) * (s + b.s);
        return new SoPhuc(fi, se);
    }

    @Override
    public String toString() {
        if (s >= 0)
            return f + " + " + s + "i";
        else return f + " - " + -s + "i";
    }
}
