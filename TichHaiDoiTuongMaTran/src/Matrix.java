import java.util.Scanner;

public class Matrix {
    private int n;
    private int m;
    private int[][] arr;

    public Matrix(int n, int m) {
        this.n = n;
        this.m = m;
        this.arr = new int[n][m];
    }

    public void nextMatrix(Scanner sc) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                this.arr[i][j] = sc.nextInt();
            }
        }
    }

    public Matrix trans(){
        Matrix b = new Matrix(m,n);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                b.arr[j][i] = arr[i][j];
            }
        }
        return b;
    }

    public String mul(Matrix b) {
        String res = "";
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < b.m; j++) {
                int tmp = 0;
                for (int k = 0; k < m; k++) {
                    tmp = tmp + arr[i][k] * b.arr[k][j];
                }
                res += tmp + " ";
            }
            res += "\n";
        }
        return res;
    }
}
