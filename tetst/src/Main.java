import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<Lop> ds = new ArrayList<>();
        int n = Integer.parseInt(sc.nextLine());
        while (n-->0)
            ds.add(new Lop(sc.nextLine(), sc.nextLine(), Integer.parseInt(sc.nextLine()), sc.nextLine()));
        Collections.sort(ds);
        n = Integer.parseInt(sc.nextLine());
        while (n-->0) {
            String gv = sc.nextLine();
            boolean ok = true;
            for (Lop i:ds) {
                if (i.getGiangvien().equals(gv)) {
                    if (ok) {
                        System.out.println("Danh sach cho giang vien " + i.getGiangvien() + ":");
                        ok = false;
                    }
                    System.out.println(i);
                }
            }
        }
    }
}