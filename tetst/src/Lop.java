public class Lop implements Comparable<Lop> {
    private String mamon, tenmon, giangvien;
    private int nhom;

    public Lop(String mamon, String tenmon, int nhom, String giangvien) {
        this.mamon = mamon;
        this.tenmon = tenmon;
        this.nhom = nhom;
        this.giangvien = giangvien;
    }

    public String getGiangvien() {
        return giangvien;
    }

    public String getMamon() {
        return mamon;
    }

    @Override
    public String toString() {
        return mamon + ' ' + tenmon + ' ' + String.format("%02d", nhom);
    }

    @Override
    public int compareTo(Lop o) {
        if (this.getMamon().equals(o.getMamon()))
            return Integer.compare(this.nhom, o.nhom);
        else
            return this.getMamon().compareTo(o.getMamon());
    }
}