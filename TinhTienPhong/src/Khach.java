import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Khach implements Comparable<Khach> {
    private String maKH;
    private String ten;
    private String soPhong;
    private Date ngayDen;
    private Date ngayDi;
    private int tienPhatSinh;

    public Khach(int maKH, String ten, String soPhong, String ngayDen, String ngayDi, int tienPhatSinh) throws ParseException {
        this.maKH = String.format("KH%02d", maKH);
        this.ten = ten;
        this.soPhong = soPhong;
        this.ngayDen = new SimpleDateFormat("dd/MM/yyyy").parse(ngayDen);
        this.ngayDi = new SimpleDateFormat("dd/MM/yyyy").parse(ngayDi);
        this.tienPhatSinh = tienPhatSinh;
    }

    private long soNgay() {
        return (ngayDi.getTime() - ngayDen.getTime()) / (1000 * 24 * 60 * 60) + 1;
    }

    public String chuanHoa(String hoTen) {
        String[] hoTenArr = hoTen.trim().split("\\s+");
        StringBuilder hoTenSb = new StringBuilder();

        for (String ten : hoTenArr) {
            hoTenSb.append(ten.substring(0, 1).toUpperCase()).append(ten.substring(1).toLowerCase()).append(" ");
        }

        return hoTenSb.toString().trim();
    }


    private Long tongTien() {
        int gia = 0;
        if (soPhong.charAt(0) == '1')
            gia = 25;
        if (soPhong.charAt(0) == '2')
            gia = 34;
        if (soPhong.charAt(0) == '3')
            gia = 50;
        if (soPhong.charAt(0) == '4')
            gia = 80;
        return soNgay() * gia + tienPhatSinh;
    }

    @Override
    public String toString() {
        return maKH + ' ' + chuanHoa(ten) + ' ' + soPhong + ' ' + soNgay() + " " + tongTien();
    }


    @Override
    public int compareTo(Khach o) {
        return o.tongTien().compareTo(tongTien());
    }
}

