import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException, ParseException {
        Scanner sc = new Scanner(new File("KHACHHANG.in"));
        ArrayList<Khach> dsk = new ArrayList<>();
        int t = Integer.parseInt(sc.nextLine());
        int count = 0;
        while(t-->0){
            count++;
            dsk.add(new Khach(count, sc.nextLine(), sc.nextLine(), sc.nextLine(), sc.nextLine(), Integer.parseInt(sc.nextLine())));
        }
        dsk.sort(null);
        dsk.forEach(System.out::println);

    }
}
