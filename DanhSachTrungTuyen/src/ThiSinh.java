public class ThiSinh implements Comparable<ThiSinh> {
    private String maTS;
    private String ten;
    private double diem1, diem2, diem3;

    public ThiSinh(String maTS, String ten, double diem1, double diem2, double diem3) {
        this.maTS = maTS;
        this.ten = ten;
        this.diem1 = diem1;
        this.diem2 = diem2;
        this.diem3 = diem3;
    }

    public String chuanHoa(String hoTen) {
        String[] hoTenArr = hoTen.trim().split("\\s+");
        StringBuilder hoTenSb = new StringBuilder();

        for (String ten : hoTenArr) {
            hoTenSb.append(ten.substring(0, 1).toUpperCase()).append(ten.substring(1).toLowerCase()).append(" ");
        }

        return hoTenSb.toString().trim();
    }

    public Double diemUuTien() {
        if (maTS.charAt(2) == '1')
            return 0.5;
        else if (maTS.charAt(2) == '2')
            return 1.0;
        else return 2.5;
    }

    public Double tinhDiem() {
        double res = diem1 * 2 + diem2 + diem3;
        return res + diemUuTien();
    }

    public String chuanHoaDiem(String diem) {
        if (diem.charAt(diem.length() - 1) == '0') {
            diem = diem.substring(0, diem.length() - 1);
        }
        if (diem.charAt(diem.length() - 1) == '.') {
            diem = diem.substring(0, diem.length() - 1);
        }
        return diem;
    }

    @Override
    public String toString() {
        return maTS + ' ' + chuanHoa(ten) + ' ' + chuanHoaDiem(diemUuTien().toString()) + ' ' + chuanHoaDiem(tinhDiem().toString());
    }

    @Override
    public int compareTo(ThiSinh o) {
        return o.tinhDiem().compareTo(tinhDiem());
    }
}
