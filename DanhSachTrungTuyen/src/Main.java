import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("THISINH.in"));
        int t = Integer.parseInt(sc.nextLine());
        ArrayList<ThiSinh> dsts = new ArrayList<>();
        while (t-- > 0) {
            dsts.add(new ThiSinh(sc.nextLine(), sc.nextLine(), Double.parseDouble(sc.nextLine()), Double.parseDouble(sc.nextLine()), Double.parseDouble(sc.nextLine())));

        }
        dsts.sort(null);
        int soTrungTuyen = Integer.parseInt(sc.nextLine());
        double diemTrungTuyen = dsts.get(soTrungTuyen-1).tinhDiem();
        System.out.println(diemTrungTuyen);
        for (ThiSinh ts : dsts) {
            System.out.print(ts);
            if (ts.tinhDiem() >= diemTrungTuyen)
                System.out.println(" TRUNG TUYEN");
            else System.out.println(" TRUOT");
        }
    }
}
