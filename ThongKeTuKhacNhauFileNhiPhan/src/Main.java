import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        FileInputStream fileInp = new FileInputStream("DATA.in");
        ObjectInputStream objIS = new ObjectInputStream(fileInp);
        ArrayList<String> a = (ArrayList<String>) objIS.readObject();
        objIS.close();

        Map<String, Integer> map = new HashMap<>();
        for (String s : a) {
            String[] words = s.toLowerCase().replaceAll("[^A-Za-z0-9]+", " ").trim().split("\\s+");
            for (String word : words) {
                if (map.containsKey(word)) {
                    map.put(word, map.get(word) + 1);
                } else map.put(word, 1);
            }
        }
        ArrayList<WordAndCount> wordList = new ArrayList<>();
        for (String word : map.keySet()) {
            WordAndCount wordAndCount = new WordAndCount(word, map.get(word));
            wordList.add(wordAndCount);
        }
        wordList.sort(null);
        wordList.forEach(System.out::println);
    }
}
