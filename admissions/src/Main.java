import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        ArrayList<Candidates> candidatesArrayList = new ArrayList<>();
        while(t-->0){
            Candidates candidates = new Candidates(sc.nextLine(), sc.nextLine(), Double.parseDouble(sc.nextLine()), Double.parseDouble(sc.nextLine()), Double.parseDouble(sc.nextLine()));
            candidatesArrayList.add(candidates);
        }
        candidatesArrayList.sort(null);
        for (Candidates candidates : candidatesArrayList) {
            candidates.display();
        }
    }
}
