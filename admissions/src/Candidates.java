public class Candidates implements Comparable<Candidates> {
    private String id;
    private String name;
    private double mathPoint;
    private double physicsPoint;
    private double chemistryPoint;
    private double pointTatol;

    public Candidates(String id, String name, Double mathPoint, Double physicsPoint, Double chemistryPoint) {
        this.id = id;
        this.name = name;
        this.mathPoint = mathPoint;
        this.physicsPoint = physicsPoint;
        this.chemistryPoint = chemistryPoint;
        this.pointTatol = mathPoint * 2 + physicsPoint + chemistryPoint;
    }

    public Double getPoint(){
        double bonus = 0;
        if (id.charAt(2) == '1')
            bonus = 0.5;
        if (id.charAt(2) == '2')
            bonus = 1;
        if (id.charAt(2) == '3')
            bonus = 2.5;
        return pointTatol + bonus;
    }

    public void display() {
        double bonus = 0;
        if (id.charAt(2) == '1')
            bonus = 0.5;
        if (id.charAt(2) == '2')
            bonus = 1;
        if (id.charAt(2) == '3')
            bonus = 2.5;
        String status = "TRUOT";
        if (pointTatol + bonus >= 24)
            status = "TRUNG TUYEN";

        if (bonus * 10 % 10 == 0) {
            Integer tmp1 = (int) bonus;
            System.out.print(id + " " + name + " " + tmp1);
        } else
            System.out.print(id + " " + name + " " + bonus);

        if (getPoint() * 10 % 10 == 0) {
            double a = getPoint();
            Integer tmp2 = (int) a;
            System.out.println(" " + tmp2 + " " + status);
        } else
            System.out.println(" " + getPoint() + " " + status);
    }


    @Override
    public int compareTo(Candidates o) {
        return o.getPoint().compareTo(getPoint());
    }
}
