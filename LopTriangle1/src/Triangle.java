public class Triangle {
    private Point p1;
    private Point p2;
    private Point p3;

    public Triangle(Point point1, Point point2, Point point3) {
        this.p1 = point1;
        this.p2 = point2;
        this.p3 = point3;
    }

    public boolean valid() {
        if (p1.distance(p2) + p1.distance(p3) <= p2.distance(p3)
                || p2.distance(p1) + p2.distance(p3) <= p1.distance(p3)
                || p3.distance(p1) + p3.distance(p2) <= p1.distance(p2))
            return false;
        return true;
    }

    public double getPerimeter() {
        double a = p1.distance(p2);
        double b = p2.distance(p3);
        double c = p1.distance(p3);
        double s = a + b + c;
        s = Math.round(s * 1000.0) / 1000.0;
        return s;
    }
}
