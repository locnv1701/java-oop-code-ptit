import java.util.Scanner;

public class Point {
    private double x, y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public static Point nextPoint(Scanner sc){
        return new Point(sc.nextDouble(), sc.nextDouble());
    }

    public double distance(Point p) {
        return distance(p, this);
    }

    public double distance(Point p1, Point p2) {
        double a = (p1.x - p2.x) * (p1.x - p2.x);
        double b = (p1.y - p2.y) * (p1.y - p2.y);
        return Math.sqrt(a + b);
    }
}