public class SinhVien implements Comparable<SinhVien> {
    private String name;
    private Integer diem;
    private String maSV;

    public SinhVien(int maSV, String name, int diem1, int diem2, int diem3) {
        this.maSV = String.format("SV%02d", maSV);
        this.name = name;
        this.diem = diem1 * 25 + diem2 * 35 + diem3 * 40;
    }

    private String xepLoai(){
        if(diem >= 800){
            return "GIOI";
        }
        else if(diem >= 650){
            return "KHA";
        }
        else if(diem >= 500){
            return "TRUNG BINH";
        }
        else return "KEM";
    }

    @Override
    public String toString() {
        return maSV + " " + chuanHoa(name) + String.format("%.2f", diem/100.0) +" "+ xepLoai();
    }

    private String chuanHoa(String s) {
        StringBuilder res = new StringBuilder();
        String[] word = s.trim().toLowerCase().split("\\s+");
        for (String value : word) {
            res.append(value.substring(0, 1).toUpperCase()).append(value.substring(1)).append(" ");
        }
        return res.toString();
    }

    @Override
    public int compareTo(SinhVien o) {
        return o.diem.compareTo(diem);
    }
}
