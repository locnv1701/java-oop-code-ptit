public class Assignments {
    private int group;
    private String content;

    public Assignments(int group, String content) {
        this.group = group;
        this.content = content;
    }

    public int getGroup() {
        return group;
    }

    @Override
    public String toString() {
        return content;
    }
}
