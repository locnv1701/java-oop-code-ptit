import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        String tmp = sc.nextLine();
        ArrayList<Student> studentArrayList = new ArrayList<>();
        while (n-- > 0) {
            Student student = new Student(sc.nextLine(), sc.nextLine(), sc.nextLine(), Integer.parseInt(sc.nextLine()));
            studentArrayList.add(student);
        }
        ArrayList<Assignments> assignmentsArrayList = new ArrayList<>();
        for (int i = 0; i < m; i++) {
            Assignments assignments = new Assignments(i + 1, sc.nextLine());
            assignmentsArrayList.add(assignments);
        }
        studentArrayList.sort(null);
        for(Student student : studentArrayList){
            System.out.print(student);
            for (Assignments assignments : assignmentsArrayList){
                if(assignments.getGroup() == student.getGroup())
                    System.out.println(assignments);
            }
        }
    }
}
