import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        ArrayList<SanPham> dssp = new ArrayList<>();
        while (t-- > 0) {
            dssp.add(new SanPham(sc.nextLine(),
                    sc.nextLine(),
                    Integer.parseInt(sc.nextLine()),
                    Integer.parseInt(sc.nextLine())));
        }
        t = Integer.parseInt(sc.nextLine());
        ArrayList<HoaDon> dshd = new ArrayList<>();
        while (t-- > 0) {
            String maHD = sc.next();
            int soLuong = Integer.parseInt(sc.next());
            SanPham sanPham = dssp.stream().filter(s -> s.getMaSP().equals(maHD.substring(0, 2))).findFirst().get();
            HoaDon hoaDon = new HoaDon(maHD, sanPham, soLuong);
            dshd.add(hoaDon);
        }
        dshd.sort(null);
        dshd.forEach(System.out::println);
    }
}
