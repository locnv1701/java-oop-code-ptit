public class HoaDon implements Comparable<HoaDon> {
    public static int stt = 0;
    private String maSP;
    private SanPham sanPham;
    private int soLuong;

    public HoaDon(String maSP, SanPham sanPham, int soLuong) {
        stt++;
        this.maSP = String.format(maSP + "-%03d", stt);
        this.sanPham = sanPham;
        this.soLuong = soLuong;
    }

    private int tinhTongTien() {
        int gia = 0;
        if (maSP.charAt(2) == '1')
            gia = sanPham.getDonGia1();
        if (maSP.charAt(2) == '2')
            gia = sanPham.getDonGia2();

        return gia * soLuong;

    }

    private int tinhTienTra() {
        int thanhTien = tinhTongTien();
        if (soLuong >= 150)
            return thanhTien / 2;
        else if (soLuong >= 100)
            return thanhTien * 70 / 100;
        else if (soLuong >= 50)
            return thanhTien * 85 / 100;
        else
            return thanhTien;
    }

    private int tinhTienGiamGia() {
        return tinhTongTien() - tinhTienTra();
    }


    @Override
    public String toString() {
        return maSP + " " + sanPham.getTen() + " " + tinhTienGiamGia() + " " + tinhTienTra();
    }

    @Override
    public int compareTo(HoaDon o) {
        Integer a = o.tinhTienTra();
        Integer b = tinhTienTra();
        return a.compareTo(b);
    }
}
