public class SanPham {
    private String maSP;
    private String ten;
    private Integer donGia1;
    private Integer donGia2;

    public SanPham(String maSP, String ten, Integer donGia1, Integer donGia2) {
        this.maSP = maSP;
        this.ten = ten;
        this.donGia1 = donGia1;
        this.donGia2 = donGia2;
    }

    public String getMaSP() {
        return maSP;
    }

    public String getTen() {
        return ten;
    }

    public Integer getDonGia1() {
        return donGia1;
    }

    public Integer getDonGia2() {
        return donGia2;
    }
}
