import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class KhachHang {
    private String maKH;
    private String name;
    private String gioiTinh;
    private Date ngaySinh;
    private String diaChi;

    public KhachHang(int stt, String name, String gioiTinh, String ngaySinh, String diaChi) throws ParseException {
        this.maKH = String.format("KH%03d", stt);
        this.name = name;
        this.gioiTinh = gioiTinh;
        this.ngaySinh = new SimpleDateFormat("dd/MM/yyyy").parse(ngaySinh);

        this.diaChi = diaChi;
    }

    public String getMaKH() {
        return maKH;
    }

    public String getName() {
        return name;
    }

    public String getGioiTinh() {
        return gioiTinh;
    }

    public Date getNgaySinh() {
        return ngaySinh;
    }

    public String getDiaChi() {
        return diaChi;
    }
}
