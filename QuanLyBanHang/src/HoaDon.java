public class HoaDon implements Comparable<HoaDon> {
    private String mHD;
    private KhachHang nguoimua;
    private MatHang sanpham;
    private int soluong;

    public HoaDon(int mHD, KhachHang nguoimua, MatHang sanpham, int soluong) {
        this.mHD = String.format("HD%03d", mHD);
        this.nguoimua = nguoimua;
        this.sanpham = sanpham;
        this.soluong = soluong;
    }

    private Integer TinhTienLai() {
        return (sanpham.getGiaBan() - sanpham.getGiaMua()) * soluong;
    }

    @Override
    public String toString() {
        return mHD + ' ' + nguoimua.getName() + " " + nguoimua.getDiaChi() + " "
                + sanpham.getTen() + " " + soluong + ' ' + sanpham.getGiaBan() * soluong + " " + TinhTienLai();
    }

    @Override
    public int compareTo(HoaDon o) {
            return o.TinhTienLai().compareTo(TinhTienLai());
    }
}
