import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class ThongKeTuKhacNhau {
    public static void main(String[] args) throws IOException {
        File file = new File("VANBAN.in");
        Scanner output = new Scanner(file);
        int t = Integer.parseInt(output.nextLine());
        Map<String, Integer> map = new HashMap<>();
        while (t-->0) {
            String[] words = output.nextLine().toLowerCase().replaceAll("[^A-Za-z0-9]+", " ").trim().split("\\s+");
            for (String word : words) {
                if (map.containsKey(word)) {
                    map.put(word, map.get(word) + 1);
                } else map.put(word, 1);
            }
        }
        ArrayList<WordAndCount> wordList = new ArrayList<>();
        for(String word : map.keySet()){
            WordAndCount wordAndCount = new WordAndCount(word, map.get(word));
            wordList.add(wordAndCount);
        }
        wordList.sort(null);
        wordList.forEach(System.out::println);
    }
}
