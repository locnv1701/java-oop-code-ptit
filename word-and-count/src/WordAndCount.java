
public class WordAndCount implements Comparable<WordAndCount> {
    private String word;
    private int count;

    public WordAndCount(String word, int count) {
        this.word = word;
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    @Override
    public String toString() {
        return word + ' ' + count;
    }

    @Override
    public int compareTo(WordAndCount o) {
        Integer a = count;
        Integer b = o.count;
        if(a.compareTo(b) != 0) {
            return b.compareTo(a);
        }
        return word.compareTo(o.word);
    }
}
