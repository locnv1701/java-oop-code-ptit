public class Staff implements Comparable<Staff> {
    private int id;
    private String name;
    private int basicSalary;
    private int numberOfWorkdays;
    private String position;


    public Staff(int id, String name, int basicSalary, int numberOfWorkdays, String position) {
        this.id = id;
        this.name = name;
        this.basicSalary = basicSalary;
        this.numberOfWorkdays = numberOfWorkdays;
        this.position = position;
    }

    public String getPosition() {
        return position;
    }

    public Integer getSalary(){
        int bonus = 0;
        if (position.equals("GD"))
            bonus = 250000;
        else if (position.equals("PGD"))
            bonus = 200000;
        else if (position.equals("TP"))
            bonus = 180000;
        else if (position.equals("NV"))
            bonus = 150000;

        int salary = (basicSalary * numberOfWorkdays);
        int thuong = 0;
        if (numberOfWorkdays >= 25)
            thuong = 20;
        else if (numberOfWorkdays >= 22)
            thuong = 10;
        thuong *= (double) salary / 100;
        return salary + bonus + thuong;
    }


    public void display() {
        int bonus = 0;
        if (position.equals("GD"))
            bonus = 250000;
        else if (position.equals("PGD"))
            bonus = 200000;
        else if (position.equals("TP"))
            bonus = 180000;
        else if (position.equals("NV"))
            bonus = 150000;

        int salary = (basicSalary * numberOfWorkdays);
        int thuong = 0;
        if (numberOfWorkdays >= 25)
            thuong = 20;
        else if (numberOfWorkdays >= 22)
            thuong = 10;
        thuong *= (double) salary / 100;

        System.out.println("NV" + String.format("%02d", id) + " " + name + " " + salary + " " + thuong + " " + bonus + " " + (salary + bonus + thuong));
    }

    @Override
    public int compareTo(Staff o) {
        return o.getSalary().compareTo(getSalary());
    }
}
