import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        int count = 0;
        ArrayList<Staff> staffArrayList = new ArrayList<>();
        while(t-->0){
            count++;
            Staff staff = new Staff(count,sc.nextLine(),Integer.parseInt(sc.nextLine()),Integer.parseInt(sc.nextLine()),sc.nextLine());
            staffArrayList.add(staff);
        }
        staffArrayList.sort(null);
        for (Staff staff : staffArrayList) {
            staff.display();
        }
    }
}
