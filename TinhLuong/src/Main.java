import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());

        ArrayList<NhanVien> dsnv = new ArrayList<>();
        ArrayList<PhongBan> dspb = new ArrayList<>();

        while (t-- > 0) {
            String[] input = sc.nextLine().split("\\s+");
            StringBuilder tenPB = new StringBuilder();
            for (int i = 1; i < input.length; i++) {
                tenPB.append(input[i]);
                tenPB.append(" ");
            }
            dspb.add(new PhongBan(input[0], tenPB.toString().trim()));
        }

        t = Integer.parseInt(sc.nextLine());
        while (t-- > 0) {
            String ma = sc.nextLine();
            dsnv.add(new NhanVien(ma,
                    sc.nextLine(),
                    Integer.parseInt(sc.nextLine()),
                    Integer.parseInt(sc.nextLine()),
                    dspb.stream().filter(s -> s.getMaPB().equals(ma.substring(3, 5))).findFirst().get()
            ));
        }
        String pb = sc.nextLine();
        boolean quinn = true;
        System.out.print("Bang luong phong ");
        for (NhanVien nhanVien : dsnv) {
            if (nhanVien.getPhongBan().getMaPB().equals(pb)){
                if (quinn){
                    System.out.println(nhanVien.getPhongBan().getTenPB() + ": ");
                    quinn = false;
                }
                System.out.println(nhanVien);
            }

        }
    }
}
