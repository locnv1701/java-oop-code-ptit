public class NhanVien {
    private String maNV;
    private String ten;
    private int luongCoBan;
    private int ngayCong;
    private PhongBan phongBan;
    private int heSo;

    public NhanVien(String maNV, String ten, int luongCoBan, int ngayCong, PhongBan phongBan) {
        this.maNV = maNV;
        this.ten = ten;
        this.luongCoBan = luongCoBan;
        this.ngayCong = ngayCong;
        this.phongBan = phongBan;
        char nhom = maNV.charAt(0);
        int nam = Integer.parseInt(maNV.substring(1, 3));
        if (nhom == 'A') {
            if (nam >= 1 && nam <= 3)
                this.heSo = 10;
            else if (nam <= 8)
                this.heSo = 12;
            else if (nam <= 15)
                this.heSo = 14;
            else this.heSo = 20;
        }
        if (nhom == 'B') {
            if (nam > 1 && nam <= 3)
                this.heSo = 10;
            else if (nam <= 8)
                this.heSo = 11;
            else if (nam <= 15)
                this.heSo = 13;
            else this.heSo = 16;
        }
        if (nhom == 'C') {
            if (nam > 1 && nam <= 3)
                this.heSo = 9;
            else if (nam <= 8)
                this.heSo = 10;
            else if (nam <= 15)
                this.heSo = 12;
            else this.heSo = 14;
        }
        if (nhom == 'D') {
            if (nam > 1 && nam <= 3)
                this.heSo = 8;
            else if (nam <= 8)
                this.heSo = 9;
            else if (nam <= 15)
                this.heSo = 11;
            else this.heSo = 13;
        }
    }

    public PhongBan getPhongBan() {
        return phongBan;
    }

    @Override
    public String toString() {
        return maNV + ' ' + ten  + " " + (luongCoBan * ngayCong * heSo * 1000);
    }
}
