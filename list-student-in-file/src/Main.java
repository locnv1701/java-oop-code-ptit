import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException, ParseException {
        File file = new File("SV.in");
        Scanner output = new Scanner(file);
        int t = Integer.parseInt(output.nextLine());
        ArrayList<Student> studentArrayList = new ArrayList<>();
        int count = 0;
        while (t-->0) {
            count++;
            Student student = new Student(count, output.nextLine(), output.nextLine(),output.nextLine(), (float) Double.parseDouble(output.nextLine()));
            studentArrayList.add(student);
        }
        studentArrayList.forEach(System.out::println);

    }
}
