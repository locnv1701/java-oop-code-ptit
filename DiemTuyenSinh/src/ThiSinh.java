public class ThiSinh implements Comparable<ThiSinh>{
    private String maTS;
    private String name;
    private double diemThi;
    private String danToc;
    private int khuVuc;

    public ThiSinh(int maTS, String name, double diemThi, String danToc, int khuVuc) {
        this.maTS = String.format("TS%02d", maTS);
        this.name = name;
        this.diemThi = diemThi;
        this.danToc = danToc;
        this.khuVuc = khuVuc;
    }

    private double tongDiem() {
        double tongDiem = diemThi;
        if (khuVuc == 1)
            tongDiem += 1.5;
        if (khuVuc == 2)
            tongDiem += 1;
        if (!danToc.equals("Kinh"))
            tongDiem += 1.5;
        return tongDiem;
    }

    private String ketQua() {
        if (tongDiem() >= 20.5F)
            return "Do";
        else return "Truot";
    }

    private String chuanHoa(String s) {
        StringBuilder res = new StringBuilder();
        String[] word = s.trim().toLowerCase().split("\\s+");
        for (String value : word) {
            res.append(value.substring(0, 1).toUpperCase()).append(value.substring(1)).append(" ");
        }
        return res.toString();
    }

    @Override
    public String toString() {
        return maTS + ' ' + chuanHoa(name)  + String.format("%.1f",tongDiem()) + " " + ketQua();
    }


    @Override
    public int compareTo(ThiSinh o) {
        Double a = o.tongDiem();
        Double b = tongDiem();
        int cmp = a.compareTo(b);
        if(cmp!=0)
            return cmp;
        return maTS.compareTo(o.maTS);
    }
}