import static java.lang.Math.round;

public class Hang {
    private String id;
    private String name;
    private String xepLoai;
    private double laiXuat;

    public Hang(String id, String name, String xepLoai) {
        this.id = id;
        this.name = name;
        this.xepLoai = xepLoai;
        if (xepLoai.equals("A")) {
            this.laiXuat = 8;
        }
        if (xepLoai.equals("B")) {
            this.laiXuat = 5;
        }
        if (xepLoai.equals("C")) {
            this.laiXuat = 2;
        }
    }

    public String getId() {
        return id;
    }

    public void display(long soLuongNhap, long donGia, long soLuongXuat){
        System.out.println(id +
                " " + name +
                " " + soLuongNhap*donGia +
                " "+ round(soLuongXuat/100.0*donGia*(100+laiXuat)));
    }
}
