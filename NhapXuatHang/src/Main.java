import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        ArrayList<Hang> hangArrayList = new ArrayList<>();
        while (t-- > 0) {
            Hang hang = new Hang(sc.nextLine(), sc.nextLine(), sc.nextLine());
            hangArrayList.add(hang);
        }
        int m = Integer.parseInt(sc.nextLine());
        while (m-- > 0) {
            String[] word =sc.nextLine().trim().split("\\s+");
            String id = word[0];
            for (Hang hang : hangArrayList) {
                if (hang.getId().equals(id)) {
                    hang.display(Integer.parseInt(word[1]),
                            Integer.parseInt(word[2]),
                            Integer.parseInt(word[3])
                    );
                }
            }
        }
    }

}

/*
2
A001
Tu lanh
A
P002
May giat
B
2
A001 500 100 300
P002 1000 1000 500

 */