import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class People implements Comparable<People>{
    private String name;
    private Date dob;

    public People(String name, String dob) throws ParseException {
        this.name = name;
        this.dob = new SimpleDateFormat("dd/MM/yyyy").parse(dob);
    }

    public String getName() {
        return name;
    }

    @Override
    public int compareTo(People o) {
        return dob.compareTo(o.dob);
    }
}
