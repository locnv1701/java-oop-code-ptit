import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws ParseException {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        ArrayList<People> listPeople = new ArrayList<>();
        while (t-- > 0) {
            People people = new People(sc.next(), sc.nextLine());
            listPeople.add(people);
        }
        listPeople.sort(null);
        System.out.println(listPeople.get(listPeople.size() - 1).getName());
        System.out.println(listPeople.get(0).getName());
    }
}
