public class DoanhNghiep implements Comparable<DoanhNghiep> {
    private String ma;
    private String ten;
    private int soLuong;

    public DoanhNghiep(String maSV, String ten, int soLuong) {
        this.ma = maSV;
        this.ten = ten;
        this.soLuong = soLuong;
    }

    @Override
    public String toString() {
        return ma + ' ' + ten + ' ' + soLuong;
    }

    @Override
    public int compareTo(DoanhNghiep o) {
        return ma.compareTo(o.ma);
    }
}
