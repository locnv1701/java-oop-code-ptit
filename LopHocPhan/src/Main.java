import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        ArrayList<Lop> dsl = new ArrayList<>();
        while (t-- > 0) {
            dsl.add(new Lop(sc.nextLine(),
                    sc.nextLine(),
                    sc.nextLine(),
                    sc.nextLine()
            ));
        }

        dsl.sort(null);
        t = Integer.parseInt(sc.nextLine());
        while (t-- > 0) {
            String ten = sc.nextLine();
            System.out.println("Danh sach cho giang vien " + ten + ": ");
            for (int i = 0; i < dsl.size(); i++) {
                if (dsl.get(i).getTenGV().equals(ten)) {
                    System.out.println(dsl.get(i).getMa() + " " + dsl.get(i).getTen() + " " + dsl.get(i).getNhomLop());
                }
            }
        }

    }
}
