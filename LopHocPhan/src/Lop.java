public class Lop implements Comparable<Lop> {
    private String ma;
    private String ten;
    private String nhomLop;
    private String tenGV;

    public Lop(String ma, String ten, String nhomLop, String tenGV) {
        this.ma = ma;
        this.ten = ten;
        this.nhomLop = nhomLop;
        this.tenGV = tenGV;
    }

    public String getMa() {
        return ma;
    }

    public String getTen() {
        return ten;
    }

    public String getNhomLop() {
        return nhomLop;
    }

    public String getTenGV() {
        return tenGV;
    }

    @Override
    public int compareTo(Lop o) {
        if (ma.compareTo(o.ma) != 0)
            return ma.compareTo(o.ma);
        return getNhomLop().compareTo(o.getNhomLop());
    }
}
