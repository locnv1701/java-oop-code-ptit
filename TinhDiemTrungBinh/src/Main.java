import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("BANGDIEM.in"));
        int t = Integer.parseInt(sc.nextLine());
        ArrayList<SinhVien> dssv = new ArrayList<>();
        int count = 0;
        while(t-->0){
            count++;
            dssv.add(new SinhVien(count, sc.nextLine(), Integer.parseInt(sc.nextLine()), Integer.parseInt(sc.nextLine()), Integer.parseInt(sc.nextLine())));
        }

        dssv.sort(null);
        int rank;
        for (int i = 0; i < dssv.size(); i++) {
            rank = i + 1;
            while(rank > 1 && Objects.equals(dssv.get(i).getDiem(), dssv.get(i - 1).getDiem()))
                rank--;
            System.out.println(dssv.get(i) + " " + rank);
        }

    }
}
