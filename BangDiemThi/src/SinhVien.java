public class SinhVien {
	private String maSV;
	private String hoTen;
	private String lop;
	private String email;

	public SinhVien(String maSV, String hoTen, String lop, String email) {
		this.maSV = maSV;
		this.hoTen = chuanHoa(hoTen.trim());
		this.lop = lop;
		this.email = email;

	}

	public String getMaSV() {
		return maSV;
	}

	private String chuanHoa(String s) {
		String[] arr = s.toLowerCase().split("\\s+");
        StringBuilder sb = new StringBuilder();
        for (String word : arr) {
            sb.append(word.substring(0, 1).toUpperCase()).append(word.substring(1)).append(" ");
        }
        return sb.toString().trim();
	}

	public String getEmail() {
		return email;
	}

	@Override
	public String toString() {
		String format = "%s %s %s";
		return String.format(format, maSV, hoTen, lop);
	}
}