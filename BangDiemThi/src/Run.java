import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Run {
	public static void main(String[] args) throws FileNotFoundException {
		
		HashMap<String, SinhVien> mapSV = new HashMap<>();
		HashMap<String, MonHoc> mapMH = new HashMap<>();
		ArrayList<BangDiem> listBD = new ArrayList<>();

		Scanner sc = new Scanner(new File("SINHVIEN.in"));
		int t = Integer.parseInt(sc.nextLine());
		for (int i = 0; i < t; i++) {
			String maSV = sc.nextLine();
			mapSV.put(maSV, new SinhVien(maSV, sc.nextLine(), sc.nextLine(), sc.nextLine()));
		}

		sc = new Scanner(new File("MONHOC.in"));
		t = Integer.parseInt(sc.nextLine());
		for (int i = 0; i < t; i++) {
			String maMon = sc.nextLine();
			mapMH.put(maMon, new MonHoc(maMon, sc.nextLine(), Integer.parseInt(sc.nextLine())));
		}

		sc = new Scanner(new File("BANGDIEM.in"));
		t = Integer.parseInt(sc.nextLine());
		for (int i = 0; i < t; i++) {
			String maSV = sc.next();
			String maMon = sc.next();
			double diem = Double.parseDouble(sc.next());
			listBD.add(new BangDiem(mapSV.get(maSV), mapMH.get(maMon), diem));
		}

		listBD.sort((a, b) -> {
			if (a.getDiem() == b.getDiem()) {
				return a.getSinhVien().getMaSV().compareTo(b.getSinhVien().getMaSV());
			}
			return b.getDiem().compareTo(a.getDiem());
		});
		sc.nextLine();
		t = Integer.parseInt(sc.nextLine());
		while (t-- > 0) {
			String maMon = sc.nextLine();
			System.out.println("BANG DIEM MON " + mapMH.get(maMon).getTenMon() + ":");
			for (BangDiem bangDiem : listBD) {
				if (bangDiem.getMonHoc().getMaMon().equals(maMon)) {
					System.out.println(bangDiem);
				}
			}
		}
		sc.close();
	}
}
