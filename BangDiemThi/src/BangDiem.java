public class BangDiem {
	private SinhVien sinhVien;
	private MonHoc monHoc;
	private Double diem;

	public BangDiem(SinhVien sinhVien, MonHoc monHoc, double diem) {
		this.sinhVien = sinhVien;
		this.monHoc = monHoc;
		this.diem = diem;

	}

	public SinhVien getSinhVien() {
		return sinhVien;
	}

	public MonHoc getMonHoc() {
		return monHoc;
	}

	public Double getDiem() {
		return diem;
	}
	
	private String fomatDiem() {
		StringBuilder sb = new StringBuilder(diem.toString());
		while (sb.charAt(sb.length() - 1) == '0') {
			sb.delete(sb.length() - 1, sb.length());
		}
		if (sb.charAt(sb.length() - 1) == '.') {
			sb.delete(sb.length() - 1, sb.length());
		}
		return sb.toString();
	}

	@Override
	public String toString() {
		return sinhVien + " " + fomatDiem();
	}
}