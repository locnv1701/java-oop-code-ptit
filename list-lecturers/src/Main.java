import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        ArrayList<Lecturers> listLecturers = new ArrayList<>();
        int count = 1;

        while (t-- > 0) {
            Lecturers lecturers = new Lecturers(count++, sc.nextLine(), sc.nextLine());
            listLecturers.add(lecturers);
        }
        int t2 = Integer.parseInt(sc.nextLine());
        while (t2-- > 0) {
            String key = sc.nextLine();
            System.out.printf("DANH SACH GIANG VIEN THEO TU KHOA %s:\n", key);
            for (Lecturers lecturers : listLecturers) {
                if (lecturers.getFullName().toLowerCase().contains(key.toLowerCase()))
                    System.out.println(lecturers);
            }
        }

    }
}
