public class Lecturers implements Comparable<Lecturers> {
    private String id;
    private String name;
    private String subject;

    public Lecturers(int id, String name, String subject) {
        this.id = String.format("GV" + "%02d", id);
        this.name = name;
        this.subject = "";
        for (String tmp : subject.toUpperCase().split(" ")) {
            this.subject += tmp.charAt(0);
        }
    }

    @Override
    public String toString() {
        return id + ' ' + name + ' ' + subject;
    }

    private String getName() {
        String[] token = name.split(" ");
        return token[token.length - 1];
    }

    public String getSubject(){
        return subject;
    }
    public String getFullName(){
        return name;
    }
    @Override
    public int compareTo(Lecturers o) {
        if (!(getName().compareTo(o.getName()) == 0)) {
            return getName().compareTo(o.getName());
        } else
            return id.compareTo(o.id);
    }
}
