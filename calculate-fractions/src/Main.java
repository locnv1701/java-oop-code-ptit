import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        while(t-->0){
            PhanSo p1 = new PhanSo(sc.nextLong(), sc.nextLong());
            PhanSo p2 = new PhanSo(sc.nextLong(), sc.nextLong());
            PhanSo p3 = PhanSo.tongBinhPhanSo(p1, p2);
            PhanSo p4 = PhanSo.D(p1,p2,p3);
            System.out.print(p3 + " ");
            System.out.println(p4);
        }
    }
}
