
public class PhanSo {
    private long tuSo;
    private long mauSo;

    public PhanSo() {
    }

    public PhanSo(long tuSo, long mauSo) {
        this.tuSo = tuSo;
        this.mauSo = mauSo;
    }

    public static long gcd(long a, long b) {
        if (b == 0)
            return a;
        return gcd(b, a % b);
    }

    public static long lcm(long a, long b) {
        return a * b / gcd(a, b);
    }

    public void rutGon() {
        long gcd = gcd(tuSo, mauSo);
        tuSo = tuSo / gcd;
        mauSo = mauSo / gcd;
    }

    public static PhanSo tongBinhPhanSo(PhanSo p, PhanSo q) {
        PhanSo s = new PhanSo();
        s.tuSo = p.tuSo * lcm(p.mauSo, q.mauSo) / p.mauSo + q.tuSo * lcm(p.mauSo, q.mauSo) / q.mauSo;
        s.mauSo = lcm(p.mauSo, q.mauSo);
        s.tuSo = s.tuSo * s.tuSo;
        s.mauSo = s.mauSo * s.mauSo;
        s.rutGon();
        return s;
    }

    public static PhanSo D(PhanSo p1, PhanSo p2, PhanSo p3){
        PhanSo s = new PhanSo();
        s.tuSo = p1.tuSo * p2.tuSo * p3.tuSo;
        s.mauSo = p1.mauSo * p2.mauSo * p3.mauSo;
        s.rutGon();
        return s;
    }

    public String toString() {
        return tuSo + "/" + mauSo;
    }

    public void display() {
        System.out.println(tuSo + "/" + mauSo);
    }
}
