import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws ParseException {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String tmp = sc.nextLine();

        List<Bill> listBill = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            String name = sc.nextLine();
            int room = sc.nextInt();
            String tmp1 = sc.nextLine();
            String start = sc.nextLine();
            String end = sc.nextLine();
            int tip = Integer.valueOf(sc.nextLine());
            Bill bill = new Bill(name,room, start, end,tip, i + 1);
            listBill.add(bill);
        }

        listBill.sort(null);
        for (Bill bill : listBill) {
            bill.display();
        }
    }
}

