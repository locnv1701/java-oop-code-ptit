public class Product implements Comparable<Product>{
    private String id;
    private int imp;
    private int price;
    private int exp;
    private long tax;

    public String getId() {
        return id;
    }

    public Product(String id, int imp) {
        this.id = id;
        this.imp = imp;
        if (id.charAt(0) == 'A') {
            this.exp = (int) Math.round(imp * 60 / 100.0);
            if (id.charAt(4) == 'Y') {
                this.price = 110000;
                this.tax = (long) exp * price * 8 / 100;
            }
            if (id.charAt(4) == 'N') {
                this.price = 135000;
                this.tax = (long) exp * price * 11 / 100;

            }
        }
        if (id.charAt(0) == 'B') {
            this.exp = (int) Math.round(imp * 70 / 100.0);
            if (id.charAt(4) == 'Y') {
                this.price = 110000;
                this.tax = (long) exp * price * 17 / 100;
            }
            if (id.charAt(4) == 'N') {
                this.price = 135000;
                this.tax = (long) exp * price * 22 / 100;

            }
        }
    }

    @Override
    public String toString() {
        return id + ' ' + imp + ' ' + exp + " " + price + " " + exp * price + " " + tax;
    }

    @Override
    public int compareTo(Product o) {
        Long a = o.tax;
        Long b = tax;
        return a.compareTo(b);
    }
}