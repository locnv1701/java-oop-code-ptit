import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static ArrayList<Tinh> dst = new ArrayList<>();

    public static void main(String[] args) throws ParseException {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());

        ArrayList<CuocGoi> dscg = new ArrayList<>();
        dst.add(new Tinh("NM", "Noi mang", 800));
        while (t-- > 0) {
            dst.add(new Tinh(sc.nextLine(), sc.nextLine(), Integer.parseInt(sc.nextLine())));
        }

        t = Integer.parseInt(sc.nextLine());
        while (t-- > 0) {
            String[] input = sc.nextLine().split("\\s+");
            dscg.add(new CuocGoi(input[0], input[1], input[2]));
        }
        dscg.sort(null);
        dscg.forEach(System.out::println);

    }
}
