public class Tinh {
    private String maVung;
    private String tenTinh;
    private int giaCuoc;

    public Tinh(String maVung, String tenTinh, int giaCuoc) {
        this.maVung = maVung;
        this.tenTinh = tenTinh;
        this.giaCuoc = giaCuoc;
    }

    public String getMaVung() {
        return maVung;
    }

    public int getGiaCuoc() {
        return giaCuoc;
    }

    public String getTenTinh() {
        return tenTinh;
    }
}
