import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CuocGoi implements Comparable<CuocGoi> {
    private String thueBaoGoi;
    private Tinh tinh;
    private Date start;
    private Date finish;
    private long time;

    public CuocGoi(String thueBaoGoi, String start, String finish) throws ParseException {
        this.thueBaoGoi = thueBaoGoi;
        this.start = new SimpleDateFormat("HH:mm").parse(start);
        this.finish = new SimpleDateFormat("HH:mm").parse(finish);
        if (thueBaoGoi.charAt(0) == '0') {
            this.tinh = Main.dst.stream().filter(s -> s.getMaVung().equals(thueBaoGoi.substring(1, 3))).findFirst().get();
            this.time = (this.finish.getTime() - this.start.getTime()) / 60000;
        } else {
            this.tinh = Main.dst.stream().filter(s -> s.getMaVung().equals("NM")).findFirst().get();
            this.time = (this.finish.getTime() - this.start.getTime()) / 60000;
            while (this.time % 3 != 0) {
                this.time++;
            }
            this.time /= 3;
        }
    }

    public Long tinhTien() {
        return time * tinh.getGiaCuoc();
    }

    @Override
    public String toString() {
        return thueBaoGoi + " " + tinh.getTenTinh() + " " + time + " " + tinhTien();

    }

    @Override
    public int compareTo(CuocGoi o) {
        return o.tinhTien().compareTo(tinhTien());
    }
}
