import static java.lang.Math.round;

public class ThiSinh implements Comparable<ThiSinh> {
    private String maTS;
    private String ten;
    private String ngaySinh;
    private double diem1;
    private double diem2;

    public ThiSinh(int count, String ten, String ngaySinh, double diem1, double diem2) {
        this.maTS = String.format("PH%02d", count);
        this.ten = ten;
        this.ngaySinh = ngaySinh;
        this.diem1 = diem1;
        this.diem2 = diem2;
    }

    private double tinhDiemCong() {
        if (diem1 >= 8 && diem2 >= 8)
            return 1;
        if (diem1 >= 7.5 && diem2 >= 7.5)
            return 0.5;
        return 0;
    }

    private Integer tinhDiem() {
        if ((int) round((diem1 + diem2) / 2 + tinhDiemCong()) > 10)
            return 10;
        return (int) round((diem1 + diem2) / 2 + tinhDiemCong());
    }

    private int tinhTuoi() {
        String[] s = ngaySinh.split("/");
        return 2021 - Integer.parseInt(s[2]);
    }

    private String xepLoai() {
        if (tinhDiem() == 9 || tinhDiem() == 10)
            return "Xuat sac";
        if (tinhDiem() == 8)
            return "Gioi";
        if (tinhDiem() == 7)
            return "Kha";
        if (tinhDiem() == 5 || tinhDiem() == 6)
            return "Trung binh";
        else return "Truot";
    }

    @Override
    public String toString() {
        return maTS + " " + ten + " " + tinhTuoi() + " " + tinhDiem() + " " + xepLoai();
    }

    @Override
    public int compareTo(ThiSinh o) {
        return -tinhDiem().compareTo(o.tinhDiem());
    }
}
