import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        ArrayList<ThiSinh> dsts = new ArrayList<>();
        int count = 0;
        while (t-- > 0) {
            count++;
            dsts.add(new ThiSinh(count, sc.nextLine(), sc.nextLine(), Double.parseDouble(sc.nextLine()), Double.parseDouble(sc.nextLine())));
        }
        dsts.sort(null);
        dsts.forEach(System.out::println);
    }
}
