public class KhachHang {
    private String maKH;
    private String ten;
    private String gioiTinh;
    private String ngaySinh;
    private String diaChi;

    public KhachHang(int maKH, String ten, String gioiTinh, String ngaySinh, String diaChi) {
        this.maKH = String.format("KH%03d", maKH);
        this.ten = ten;
        this.gioiTinh = gioiTinh;
        this.ngaySinh = ngaySinh;
        this.diaChi = diaChi;
    }

    public String getMaKH() {
        return maKH;
    }
}
