public class MatHang {
    private String maMH;
    private String ten;
    private String donVi;
    private int giaBan;
    private int giaMua;

    public MatHang(int maMH, String ten, String donVi, int giaBan, int giaMua) {
        this.maMH = String.format("MH%03d", maMH);
        this.ten = ten;
        this.donVi = donVi;
        this.giaBan = giaBan;
        this.giaMua = giaMua;
    }

    public String getMaMH() {
        return maMH;
    }
}
