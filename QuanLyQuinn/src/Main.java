import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    static ArrayList<KhachHang> dskh = new ArrayList<>();
    static ArrayList<MatHang> dsmh = new ArrayList<>();
    static ArrayList<HoaDon> dshd = new ArrayList<>();

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        int count = 0;
        while (t-- > 0) {
            count++;
            KhachHang khachHang = new KhachHang(count, sc.nextLine(), sc.nextLine(), sc.nextLine(), sc.nextLine());
            dskh.add(khachHang);
        }
        t = Integer.parseInt(sc.nextLine());
        count = 0;
        while (t-- > 0) {
            count++;
            MatHang matHang = new MatHang(count, sc.nextLine(), sc.nextLine(), Integer.parseInt(sc.nextLine()), Integer.parseInt(sc.nextLine()));
            dsmh.add(matHang);
        }

        t = Integer.parseInt(sc.nextLine());
        count = 0;
        while (t-- > 0) {
            count++;
            String maKH = sc.next();
            String maMH = sc.next();
            int soLuong = sc.nextInt();

            MatHang matHang = null;
            KhachHang khachHang = null;
            for (KhachHang a : dskh) {
                if (maKH.equals(a.getMaKH())) {
                    khachHang = a;
                    break;
                }
            }

            for (MatHang a : dsmh) {
                if (maMH.equals(a.getMaMH())) {
                    matHang = a;
                    break;
                }
            }

            HoaDon hoaDon = new HoaDon(count, khachHang, matHang, soLuong);
            dshd.add(hoaDon);
        }
        dshd.forEach(System.out::println);
    }
}