public class HoaDon {
    private String ma;
    private KhachHang khachHang;
    private MatHang matHang;
    private int soLuong;

    public HoaDon(int ma, KhachHang khachHang, MatHang matHang, int soLuong) {
        this.ma = String.format("HD%03d", ma);
        this.khachHang = khachHang;
        this.matHang = matHang;
        this.soLuong = soLuong;
    }
}
