import java.util.TreeSet;

public class IntSet {
    private TreeSet<Integer> treeSet = new TreeSet<>();

    public IntSet(int[] arr) {
        for (int j : arr) {
            this.treeSet.add(j);
        }
    }

    public IntSet intersection(IntSet s2) {
        TreeSet<Integer> res = new TreeSet<>();
        treeSet.forEach(a -> {
            if (s2.treeSet.contains(a)) res.add(a);
        });
        int[] arr = new int[res.size()];
        Object[] arrayList = res.toArray();
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) arrayList[i];
        }
        return new IntSet(arr);
    }

    @Override
    public String toString() {
        return treeSet.toString().replace("[", "").replace("]", "").replace(",", "");
    }
}
