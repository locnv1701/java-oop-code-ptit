import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("KHACHHANG.in"));

        int soHo = Integer.parseInt(scanner.nextLine());
        ArrayList<Ho> dsh = new ArrayList<>();
        int soThuTu = 0;
        while (soHo-- > 0) {
            String tenChuHo = scanner.nextLine();
            String[] thongTinSoDien = scanner.nextLine().trim().split("\\s+");

            dsh.add(new Ho(
                    ++soThuTu,
                    tenChuHo,
                    thongTinSoDien[0],
                    Integer.parseInt(thongTinSoDien[1]),
                    Integer.parseInt(thongTinSoDien[2])
            ));
        }
        dsh.sort(null);
        dsh.forEach(System.out::println);
    }
}
