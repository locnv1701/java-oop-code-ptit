public class Ho implements Comparable<Ho>{
    private String maKH;
    private String tenChuHo;
    private String loaiHo;
    private int chiSoDau;
    private int chiSoCuoi;
    private int tienTrongDM;
    private int tienVuotDM;
    private double VAT;
    private Double tienPhaiNop;

    public Ho(int soThuTu, String tenChuHo, String loaiHo, int chiSoDau, int chiSoCuoi) {
        this.maKH = "KH" + String.format("%02d", soThuTu);
        this.tenChuHo = chuanHoaTen(tenChuHo);
        this.loaiHo = loaiHo;
        this.chiSoDau = chiSoDau;
        this.chiSoCuoi = chiSoCuoi;
        tinhTien();
    }

    private void tinhTien() {
        int dinhMuc = 0;
        switch (loaiHo) {
            case "A":
                dinhMuc = 100;
                break;
            case "B":
                dinhMuc = 500;
                break;
            case "C":
                dinhMuc = 200;
                break;
        }

        int soDien = chiSoCuoi - chiSoDau;
        if (soDien < dinhMuc) {
            this.tienTrongDM = soDien * 450;
            this.tienVuotDM = 0;
        } else {
            this.tienTrongDM = dinhMuc * 450;
            this.tienVuotDM = (soDien - dinhMuc) * 1000;
        }

        this.VAT = (tienVuotDM * 0.05);

        this.tienPhaiNop = this.tienTrongDM + this.tienVuotDM + this.VAT;
    }

    private String chuanHoaTien(double tien) {
        if (tien == (int) tien) {
            return String.valueOf((int) tien);
        }
        return String.valueOf(tien);
    }

    private String chuanHoaTen(String str) {
        str = str.toLowerCase();
        str = str.trim();
        str = str.replaceAll("\\s+", " ");
        String[] token = str.split(" ");
        StringBuilder res = new StringBuilder();
        for (String s : token) {
            res.append(String.valueOf(s.charAt(0)).toUpperCase()).append(s.substring(1));
            res.append(" ");
        }
        return res.toString().trim();
    }

    @Override
    public String toString() {
        return maKH + " " + tenChuHo + " " + tienTrongDM + " " + tienVuotDM + " " + chuanHoaTien(VAT) + " " + chuanHoaTien(tienPhaiNop);
    }

    @Override
    public int compareTo(Ho o) {
        return o.tienPhaiNop.compareTo(tienPhaiNop);
    }
}
