import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        ArrayList<SinhVien> dssv = new ArrayList<>();
        ArrayList<Mon> dsm = new ArrayList<>();
        ArrayList<BangDiem> dsbd = new ArrayList<>();
        Scanner sc = new Scanner(new File("SINHVIEN.in"));

        int t = Integer.parseInt(sc.nextLine());
        while (t-- > 0) {
            dssv.add(new SinhVien(sc.nextLine(), sc.nextLine(), sc.nextLine(), sc.nextLine()));
        }
        sc = new Scanner(new File("MONHOC.in"));
        t = Integer.parseInt(sc.nextLine());
        while (t-- > 0) {
            dsm.add(new Mon(sc.nextLine(), sc.nextLine(), sc.nextLine()));
        }

        sc = new Scanner(new File("BANGDIEM.in"));
        t = Integer.parseInt(sc.nextLine());
        while (t-- > 0) {
            String[] input = sc.nextLine().split("\\s+");
            dsbd.add(new BangDiem(
                    dssv.stream().filter(s -> s.getMa().equals(input[0])).findFirst().get(),
                    dsm.stream().filter(s -> s.getMa().equals(input[1])).findFirst().get(),
                    Double.parseDouble(input[2])
            ));
        }

        dsbd.sort(null);

        t = Integer.parseInt(sc.nextLine());
        while (t-- > 0) {
            String lop = sc.nextLine();
            System.out.printf("BANG DIEM lop %s:\n", lop);
            for (BangDiem bangDiem : dsbd) {
                if (bangDiem.getSinhVien().getLop().equals(lop)) {
                    System.out.println(bangDiem);
                }
            }
        }
    }
}
