public class Mon {
    private String ma, ten, tinchi;

    public Mon(String ma, String ten, String tinchi) {
        this.ma = ma;
        this.ten = ten;
        this.tinchi = tinchi;
    }

    public String getMa() {
        return ma;
    }

    public String getTen() {
        return ten;
    }

    public String getTinchi() {
        return tinchi;
    }
}
