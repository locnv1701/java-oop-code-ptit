import java.util.Locale;

public class BangDiem implements Comparable<BangDiem> {
    private SinhVien sinhVien;
    private Mon mon;
    private Double diem;

    public BangDiem(SinhVien sinhVien, Mon mon, double diem) {
        this.sinhVien = sinhVien;
        this.mon = mon;
        this.diem = diem;
    }

    public SinhVien getSinhVien() {
        return sinhVien;
    }

    public Mon getMon() {
        return mon;
    }

    public double getDiem() {
        return diem;
    }

    private String chuanHoaTen(String ten) {
        String[] word = ten.toLowerCase().trim().split("\\s+");
        StringBuilder res = new StringBuilder();
        for (String s : word) {
            res.append(s.substring(0, 1).toUpperCase(Locale.ROOT)).append(s.substring(1)).append(" ");
        }
        return res.toString().trim();
    }

    private String chuanHoaDiem(double diem) {
        StringBuilder s = new StringBuilder(String.valueOf(diem));
        while (s.charAt(s.length() - 1) == '0') {
            s.delete(s.length() - 1, s.length());
        }
        while (s.charAt(s.length() - 1) == '.') {
            s.delete(s.length() - 1, s.length());
        }
        return s.toString();
    }

    @Override
    public String toString() {
        return sinhVien.getMa() + " " + chuanHoaTen(sinhVien.getTen()) + " " + mon.getMa() + " " + mon.getTen()  + " " + chuanHoaDiem(diem);
    }

    @Override
    public int compareTo(BangDiem o) {
        if (o.mon.getMa().compareTo(mon.getMa()) != 0)
            return mon.getMa().compareTo(o.mon.getMa());
        return sinhVien.getMa().compareTo(o.sinhVien.getMa());
    }
}
