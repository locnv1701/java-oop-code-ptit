//import java.util.ArrayList;
//import java.util.Scanner;
//
//public class Main {
//    public static void main(String[] args) {
//        Scanner sc = new Scanner(System.in);
//        int t = Integer.parseInt(sc.nextLine());
//        while (t-- > 0) {
//            ArrayList<Point> dsd = new ArrayList<>();
//            int n = Integer.parseInt(sc.nextLine());
//            while (n-- > 0) {
//                String[] input = sc.nextLine().split("\\s+");
//                dsd.add(new Point(Double.parseDouble(input[0]), Double.parseDouble(input[1])));
//            }
////            ArrayList<Triangle> dstg = new ArrayList<>();
//            double tongDienTich = 0.0;
//            for (int i = 2; i < dsd.size(); i++) {
//                tongDienTich += (new Triangle(dsd.get(0), dsd.get(i - 1), dsd.get(i))).getArea();
//            }
//            System.out.printf("%.3f\n", tongDienTich);
//
//        }
//    }
//}

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner in = new Scanner(new File("POLYGON.in"));
        int t = in.nextInt();
        while(t-->0){
            int n = in.nextInt();
            Point p[] = new Point[n];
            for(int i = 0; i < n; i++){
                p[i] = new Point(in.nextInt(),in.nextInt());
            }
            Polygon poly = new Polygon(p);
            System.out.println(poly.getArea());
        }
    }
}