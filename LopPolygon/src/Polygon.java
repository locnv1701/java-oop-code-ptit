public class Polygon {
    private Point p[];

    public Polygon(Point[] p) {
        this.p = p;
    }

    public String getArea() {
        double tongDienTich = 0.0;
        for (int i = 2; i < p.length; i++) {
            tongDienTich += (new Triangle(p[0], p[i - 1], p[i])).getArea();
        }
        return String.format("%.3f\n", tongDienTich);
    }

}
