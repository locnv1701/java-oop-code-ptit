import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = Integer.parseInt(sc.nextLine());
        List<Athlete> athleteList = new ArrayList<>();
        List<Integer> time = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            Athlete athlete = new Athlete(i + 1, sc.nextLine(), sc.nextLine(), sc.nextLine(), sc.nextLine());
            athleteList.add(athlete);
            time.add(athlete.getTimeToRank());
        }

        for (Athlete athlete : athleteList) {
            int rank = 1;
            rank += time.stream().filter(t -> t < athlete.getTimeToRank()).count();
            athlete.setRank(rank);
        }
        athleteList.sort(null);
        athleteList.forEach(System.out::println);
    }
}
