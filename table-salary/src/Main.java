import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        ArrayList<Staff> staffArrayList = new ArrayList<>();
        int count = 0;
        while (t-- > 0) {
            count++;
            Staff staff = new Staff(count, sc.nextLine(), Integer.parseInt(sc.nextLine()), Integer.parseInt(sc.nextLine()), sc.nextLine());
            staffArrayList.add(staff);
        }
        int salaryTotal = 0;
        for (Staff staff : staffArrayList) {
            salaryTotal += staff.display();
        }
        System.out.println("Tong chi phi tien luong: " + salaryTotal);
    }
}
