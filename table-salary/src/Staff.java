public class Staff {
    private int id;
    private String name;
    private String position;
    private int basicSalary;
    private int numberOfWorkdays;

    public Staff(int id, String name, int basicSalary, int numberOfWorkdays, String position) {
        this.id = id;
        this.name = name;
        this.position = position;
        this.basicSalary = basicSalary;
        this.numberOfWorkdays = numberOfWorkdays;
    }

    private Integer getBonus() {
        int bonus = 0;
        if (position.equals("GD"))
            bonus = 250000;
        else if (position.equals("PGD"))
            bonus = 200000;
        else if (position.equals("TP"))
            bonus = 180000;
        else
            bonus = 150000;
        return bonus;
    }

    public int display() {
        int thuong = 0;
        int luongThang = basicSalary * numberOfWorkdays;
        if (numberOfWorkdays >= 25) {
            thuong = 20 * luongThang / 100;
        } else if (numberOfWorkdays >= 22) {
            thuong = 10 * luongThang / 100;
        }
        System.out.println("NV" + String.format("%02d", id) + " " + name + " " + basicSalary * numberOfWorkdays + " " + thuong + " " + getBonus() + " " + (basicSalary * numberOfWorkdays + thuong + getBonus()));
        return (basicSalary * numberOfWorkdays + thuong + getBonus());
    }
}
