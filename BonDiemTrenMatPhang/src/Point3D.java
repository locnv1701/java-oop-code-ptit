public class Point3D {
    private int x;
    private int y;
    private int z;

    public Point3D(int a, int b, int c) {
        this.x = a;
        this.y = b;
        this.z = c;
    }

    public static Point3D Vector(Point3D p1, Point3D p2) {
        return new Point3D(p1.x - p2.x, p1.y - p2.y, p1.z - p2.z);
    }

    public static boolean check(Point3D p1, Point3D p2, Point3D p3, Point3D p4) {
        int x1 = Vector(p1, p2).x;
        int y1 = Vector(p1, p2).y;
        int z1 = Vector(p1, p2).z;
        int x2 = Vector(p1, p3).x;
        int y2 = Vector(p1, p3).y;
        int z2 = Vector(p1, p3).z;
        int x3 = Vector(p1, p4).x;
        int y3 = Vector(p1, p4).y;
        int z3 = Vector(p1, p4).z;

        return (y1 * z2 - z1 * y2) * x3 + (z1 * x2 - x1 * z2) * y3 + (x1 * y2 - y1 * x2) * z3 == 0;
    }

}
