public class Entrprise implements Comparable<Entrprise> {
    private String id;
    private String name;
    private Integer number;

    public Entrprise(String id, String name, int number) {
        this.id = id;
        this.name = name;
        this.number = number;
    }

    @Override
    public String toString() {
        return id + ' ' +name + ' ' + number;
    }

    public Integer getNumber() {
        return number;
    }

    @Override
    public int compareTo(Entrprise o) {
        if(o.number.compareTo(number)==0){
            return id.compareTo(o.id);
        }
        return o.number.compareTo(number);
    }
}
