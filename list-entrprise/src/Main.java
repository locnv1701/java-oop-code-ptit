import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        ArrayList<Entrprise> listEntrprise = new ArrayList<>();
        while (t-- > 0) {
            Entrprise entrprise = new Entrprise(sc.nextLine(), sc.nextLine(), Integer.parseInt(sc.nextLine()));
            listEntrprise.add(entrprise);
        }
        listEntrprise.sort(null);
        int t2 = Integer.parseInt(sc.nextLine());
        while (t2-- > 0) {
            int a = sc.nextInt();
            int b = sc.nextInt();

            System.out.printf("DANH SACH DOANH NGHIEP NHAN TU %d DEN %d SINH VIEN:\n", a, b);
            for (Entrprise entrprise : listEntrprise) {
                if (entrprise.getNumber() <= b && entrprise.getNumber() >= a)
                    System.out.println(entrprise);
            }
        }

    }
}
