import java.util.*;

public class WordSet {
    private final String line;

    public WordSet(String line) {
        this.line = line;
    }

    private List<String> solve() {
        String[] words = line.toLowerCase().split("\\s+");
        Set<String> set = new HashSet<>();
        for (String word : words) {
            set.add(word);
        }
        List<String> res = new ArrayList<>(set);
        Collections.sort(res);
        return res;
    }

    public String union(WordSet s2){
        Set<String> set = new HashSet<>();
        set.addAll(this.solve());
        set.addAll(s2.solve());
        List<String> res = new ArrayList<>(set);
        Collections.sort(res);
        String result = "";
        for (String ele : res) {
            result += ele;
            result += " ";
        }
        return result;
    }

    public String intersection(WordSet s2){
        Set<String> set = new HashSet<>(this.solve());
        Set<String> res = new HashSet<>();
        s2.solve().forEach(a -> {
            if(set.contains(a))
                res.add(a);
        });
        List<String> list = new ArrayList<>(res);
        Collections.sort(list);
        String result = "";
        for (String ele : list) {
            result += ele;
            result += " ";
        }
        return result;
    }

}
