public class FootballClub implements Comparable<FootballClub> {
    private String id;
    private String name;
    private Long price;
    private int number = 0;
    private String idMatch;

    public FootballClub(String id, String name, Long price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public String getId() {
        return id;
    }


    public void display() {
        System.out.println(idMatch + ' ' + name + ' ' + price * number);
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setIdMatch(String idMatch) {
        this.idMatch = idMatch;
    }

    @Override
    public int compareTo(FootballClub o) {
        Long a = o.price * o.number;
        Long b = price * number;
        return a.compareTo(b);
    }
}
