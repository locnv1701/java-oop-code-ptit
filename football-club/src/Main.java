import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        ArrayList<FootballClub> footballClubArrayList = new ArrayList<>();
        while(t-->0){
            FootballClub fc = new FootballClub(sc.nextLine(), sc.nextLine(), Long.parseLong(sc.nextLine()));
            footballClubArrayList.add(fc);
        }
        int t2 = Integer.parseInt(sc.nextLine());
        while(t2-->0){
            String infoMatch = sc.nextLine();
            String[] word = infoMatch.split("\\s+");
            String idMatch = word[0].substring(1,3);
            int number = Integer.parseInt(word[1]);

            for (FootballClub footballClub : footballClubArrayList) {
                if(footballClub.getId().equals(idMatch)) {
                    footballClub.setNumber(number);
                    footballClub.setIdMatch(word[0]);
                }
            }
            footballClubArrayList.sort(null);
        }
        for (FootballClub footballClub : footballClubArrayList) {
            footballClub.display();
        }
    }
}
