public class SinhVien {
    private String ten;
    private double TBC;
    private int RL;
    private String hocBong;

    public SinhVien(String name, double TBC, int RL) {
        this.ten = name;
        this.TBC = TBC;
        this.RL = RL;
    }

    public String getTen() {
        return ten;
    }

    public int getRL() {
        return RL;
    }

    public double getTBC() {
        return TBC;
    }

    private String getHocBong() {
        if (TBC >= 3.6 && RL >= 90)
            return "XUATSAC";
        else if (TBC >= 3.2 && RL >= 80)
            return "GIOI";
        else if (TBC >= 2.5 && RL >= 70)
            return "KHA";
        return "KHONG";
    }

    @Override
    public String toString() {
        return ten + ": " + getHocBong();
    }
}
