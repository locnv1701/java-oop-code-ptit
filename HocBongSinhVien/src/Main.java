import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc;
        try {
            sc = new Scanner(new File("HOCBONG1.in"));
        } catch (Exception e) {
            sc = new Scanner(System.in);
        }
        String[] input = sc.nextLine().trim().split("\\s+");
        int t = Integer.parseInt(input[0]);
        int soLuong = Integer.parseInt(input[1]);
        ArrayList<SinhVien> dssv = new ArrayList<>();
        ArrayList<Double> dsd = new ArrayList<>();
        while (t-- > 0) {
            String ten = sc.nextLine();
            input = sc.nextLine().split("\\s+");
            Double TBC = Double.parseDouble(input[0]);
            int RL = Integer.parseInt(input[1]);
            if (RL >= 70) {
                dsd.add(TBC);
            }
            dssv.add(new SinhVien(ten, TBC, RL));
        }

        dsd.sort(Collections.reverseOrder());
        double diemSan = dsd.get(soLuong - 1);
//        dsd.forEach(System.out::println);

        for (SinhVien sinhVien : dssv) {
            if (sinhVien.getTBC() >= diemSan && sinhVien.getRL() >= 70) {
                System.out.println(sinhVien);
            } else {
                System.out.println(sinhVien.getTen() + ": KHONG");
            }
        }
    }
}
