public class Hang implements Comparable<Hang> {
    private String maMH;
    private String ten;
    private String nhomHang;
    private double giaMua;
    private double giaBan;

    public Hang(int maMH, String ten, String nhomHang, double giaMua, double giaBan) {
        this.maMH = String.format("MH%02d", maMH);
        this.ten = ten;
        this.nhomHang = nhomHang;
        this.giaMua = giaMua;
        this.giaBan = giaBan;
    }

    public Double loiNhuan() {
        return giaBan - giaMua;
    }

    @Override
    public String toString() {
        return maMH + ' ' + ten + ' ' + nhomHang + ' ' + String.format("%.2f",loiNhuan());
    }

    @Override
    public int compareTo(Hang o) {
        return o.loiNhuan().compareTo(loiNhuan());
    }
}
