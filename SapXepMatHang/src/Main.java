import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("MATHANG.in"));
        ArrayList<Hang> dsh = new ArrayList<>();
        int t = Integer.parseInt(sc.nextLine());
        int count = 0;
        while(t-->0){
            count++;
            dsh.add(new Hang(count, sc.nextLine(),sc.nextLine(), Double.parseDouble(sc.nextLine()), Double.parseDouble(sc.nextLine())));
        }
        dsh.sort(null);
        dsh.forEach(System.out::println);

    }
}
