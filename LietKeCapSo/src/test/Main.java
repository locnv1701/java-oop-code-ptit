package test;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Main {
    public static boolean NTCNDQ(int a, int b) {
        if (b == 0)
            return (a == 1);
        return NTCNDQ(b, a % b);
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        FileInputStream fileInp = new FileInputStream("DATA.in");
        ObjectInputStream objIS = new ObjectInputStream(fileInp);
        ArrayList<Pair> pairArrayList = (ArrayList<Pair>) objIS.readObject();
        System.out.println();
        pairArrayList.sort(null);
        Set<String> set = new HashSet<>();
        for (Pair pair : pairArrayList) {
//            System.out.println(NTCNDQ(pair.getSecond(),pair.getFirst()));
            if (pair.getSecond() <= pair.getFirst() || !NTCNDQ(pair.getSecond(),pair.getFirst()))
                continue;
            if (!set.contains(pair.toString())) {
                System.out.println(pair);
                set.add(pair.toString());
            }
        }
    }
}
/*
      ArrayList<Pair> pairs = new ArrayList<>();
        pairs.add(new Pair(1, 2));
        pairs.add(new Pair(2, 3));
        pairs.add(new Pair(1, 4));
        pairs.add(new Pair(3, 5));
        pairs.add(new Pair(5, 2));
        pairs.add(new Pair(1, 4));
        try {   // dat try cacth de tranh ngoai le khi tao va viet File
            FileOutputStream f = new FileOutputStream("DATA.in"); // tao file f tro den student.dat
            ObjectOutputStream oStream = new ObjectOutputStream(f); // dung de ghi theo Object vao file f
            oStream.writeObject(pairs); // ghi MyStudent theo kieu Object vao file
            oStream.close();
        } catch (IOException e) {
            System.out.println("Error Write file");
        }

 */