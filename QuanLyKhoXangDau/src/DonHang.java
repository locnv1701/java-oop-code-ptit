public class DonHang implements Comparable<DonHang> {
    private String ma;
    private BangGia bangGia;
    private BangHang bangHang;
    private int soLuong;

    public DonHang(String ma, BangGia bangGia, BangHang bangHang, int soLuong) {
        this.ma = ma;
        this.bangGia = bangGia;
        this.bangHang = bangHang;
        this.soLuong = soLuong;
    }

    private Long tinhTien() {
        return (long) soLuong * bangGia.getDonGia();
    }

    private long thue() {
        if (ma.substring(3, 5).equals("TN"))
            return 0;
        return (long) (bangGia.getThue() * tinhTien());
    }

    @Override
    public String toString() {
        return ma + " " + bangHang.getTen() + " " + bangGia.getDonGia() + " " + thue() + " " + (tinhTien() + thue());
    }

    @Override
    public int compareTo(DonHang o) {
        return o.tinhTien().compareTo(tinhTien());
    }
}
