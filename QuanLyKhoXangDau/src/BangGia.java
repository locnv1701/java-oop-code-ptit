public class BangGia {
    private String ma;
    private String matHang;
    private int donGia;
    private double thue;

    public BangGia(String ma, String matHang, int donGia, double thue) {
        this.ma = ma;
        this.matHang = matHang;
        this.donGia = donGia;
        this.thue = thue;
    }

    public double getThue() {
        return thue;
    }

    public String getMa() {
        return ma;
    }

    public String getMatHang() {
        return matHang;
    }

    public int getDonGia() {
        return donGia;
    }
}
