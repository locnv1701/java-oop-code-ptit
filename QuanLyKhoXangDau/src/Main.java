import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<BangGia> dsbg = new ArrayList<>();
        ArrayList<BangHang> dsbh = new ArrayList<>();
        ArrayList<DonHang> dsdh = new ArrayList<>();

        dsbg.add(new BangGia("X", "Xang", 128000, 0.03));
        dsbg.add(new BangGia("D", "Dau", 11200, 0.035));
        dsbg.add(new BangGia("N", "Nhot", 9700, 0.02));

        dsbh.add(new BangHang("BP", "British Petro"));
        dsbh.add(new BangHang("ES", "Esso"));
        dsbh.add(new BangHang("SH", "Shell"));
        dsbh.add(new BangHang("CA", "Castrol"));
        dsbh.add(new BangHang("MO", "Mobil"));
        dsbh.add(new BangHang("TN", "Trong Nuoc"));

        int t = Integer.parseInt(sc.nextLine());
        while (t-- > 0) {
            String[] input = sc.nextLine().split(" ");
            String ma = input[0];
            int soLuong = Integer.parseInt(input[1]);
            dsdh.add(new DonHang(ma,
                    dsbg.stream().filter(s -> s.getMa().equals(ma.substring(0, 1))).findFirst().get(),
                    dsbh.stream().filter(s -> s.getMa().equals(ma.substring(3, 5))).findFirst().get(),
                    soLuong
            ));
        }
        dsdh.sort(null);
        dsdh.forEach(System.out::println);
    }
}
