import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList<MatHang> dsmh = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        int count = 0;
        while (t-- > 0) {
            count++;
            MatHang matHang = new MatHang(count, sc.nextLine(), sc.nextLine(), Double.parseDouble(sc.nextLine()), Double.parseDouble(sc.nextLine()));
            dsmh.add(matHang);
        }
        dsmh.sort(null);
        dsmh.forEach(System.out::println);
    }
}
