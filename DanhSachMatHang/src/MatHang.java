public class MatHang implements Comparable<MatHang> {
    private int maMH;
    private String ten;
    private String hang;
    private double gia1;
    private double gia2;


    public MatHang(int maMH, String ten, String hang, double gia1, double gia2) {
        this.maMH = maMH;
        this.ten = ten;
        this.hang = hang;
        this.gia1 = gia1;
        this.gia2 = gia2;
    }

    private Double loiNhuan() {
        return gia2 - gia1;
    }

    @Override
    public int compareTo(MatHang o) {
        return -loiNhuan().compareTo(o.loiNhuan());
    }

    @Override
    public String toString() {
        return maMH + " " + ten + ' ' + hang + "  " + String.format("%.2f", loiNhuan());
    }
}
