public class SanPham implements Comparable<SanPham>{
    private String ma;
    private String ten;
    private Integer giaBan;
    private Integer han;

    public SanPham(String ma, String ten, String giaBan, String han) {
        this.ma = ma;
        this.ten = ten;
        this.giaBan = Integer.parseInt(giaBan);
        this.han = Integer.parseInt(han);
    }

    @Override
    public String toString() {
        return String.format("%s %s %d %d", ma, ten, giaBan, han);
    }

    @Override
    public int compareTo(SanPham o) {
        return o.giaBan.compareTo(giaBan) != 0 ? o.giaBan.compareTo(giaBan) : ma.compareTo(o.ma);
    }
}
