import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("SANPHAM.in"));
        int t = Integer.parseInt(sc.nextLine());
        ArrayList<SanPham> dssp = new ArrayList<>();
        while(t-->0){
            dssp.add(new SanPham(sc.nextLine(), sc.nextLine(), sc.nextLine(), sc.nextLine()));
        }
        dssp.sort(null);
        dssp.forEach(System.out::println);
    }
}
