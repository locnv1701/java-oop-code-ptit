import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        ArrayList<Student> studentArrayList = new ArrayList<>();
        while(t-->0){
            Student student = new Student(sc.nextLine(), sc.nextInt(), sc.nextInt());
            String tmp = sc.nextLine();
            studentArrayList.add(student);
        }
        studentArrayList.sort(null);
        studentArrayList.forEach(System.out::println);
    }
}
