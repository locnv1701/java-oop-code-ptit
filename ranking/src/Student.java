public class Student implements Comparable<Student>{
    private String name;
    private Integer AC;
    private Integer total;

    public Student(String name, int AC, int total) {
        this.name = name;
        this.AC = AC;
        this.total = total;
    }

    @Override
    public int compareTo(Student o) {
        if(o.AC.compareTo(AC) != 0)
            return o.AC.compareTo(AC);
        if(total.compareTo(o.total)!= 0)
            return total.compareTo(o.total);
        return name.compareTo(o.name);
    }

    @Override
    public String toString() {
        return name + ' ' + AC +" " + total;
    }
}
