public class Student {
    private String id;
    private String name;
    private String grade;
    private String attendance;
    private int point = 10;

    public Student(String id, String name, String grade) {
        this.id = id;
        this.name = name;
        this.grade = grade;
    }

    public String getId() {
        return id;
    }

    public void setAttendance(String attendance) {
        this.attendance = attendance;
    }

    private void getPoint() {
        for (int i = 0; i < attendance.length(); i++) {
            if (attendance.charAt(i) == 'm')
                this.point -= 1;
            if (attendance.charAt(i) == 'v')
                this.point -= 2;
        }
    }

    public String getGrade() {
        return grade;
    }

    @Override
    public String toString() {
        getPoint();
        return id + ' ' + name + ' ' + grade + ' ' +
                 ((point <= 0) ? "0 KDDK" : point);
    }
}
