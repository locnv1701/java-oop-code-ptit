import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        int m = t;
        ArrayList<Student> studentArrayList = new ArrayList<>();
        while(t-->0){
            Student student = new Student(sc.nextLine(), sc.nextLine(), sc.nextLine());
            studentArrayList.add(student);
        }

        while(m-->0){
            String id = sc.next();
            studentArrayList.forEach(student -> {
                if(student.getId().compareTo(id)==0)
                    student.setAttendance(sc.nextLine());
            });
        }
        String grade = sc.nextLine();
        studentArrayList.forEach(student -> {
            if(student.getGrade().equals(grade))
                System.out.println(student);
        });
    }
}
