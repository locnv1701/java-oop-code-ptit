public class HoaDon {
    private String mHD;
    private KhachHang nguoimua;
    private MatHang sanpham;
    private int soluong;

    public HoaDon(int mHD, KhachHang nguoimua, MatHang sanpham, int soluong) {
        this.mHD = String.format("HD%03d", mHD);
        this.nguoimua = nguoimua;
        this.sanpham = sanpham;
        this.soluong = soluong;
    }

    @Override
    public String toString() {
        return mHD + ' '
                + nguoimua.getName() + " " + nguoimua.getDiaChi() + " "
                + sanpham.getTen() + " " + sanpham.getDonVi() + " " + sanpham.getGiaMua() + " " + sanpham.getGiaBan() + " "
                + soluong + ' ' + sanpham.getGiaBan() * soluong;
    }
}
