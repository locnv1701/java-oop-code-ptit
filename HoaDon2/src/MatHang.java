public class MatHang {
    private String maMH;
    private String ten;
    private String donVi;
    private int giaMua;
    private int giaBan;

    public MatHang(int stt, String ten, String donVi, int giaMua, int giaBan) {
        this.maMH = String.format("MH%03d", stt);
        this.ten = ten;
        this.donVi = donVi;
        this.giaMua = giaMua;
        this.giaBan = giaBan;
    }

    public String getMaMH() {
        return maMH;
    }

    public String getTen() {
        return ten;
    }

    public String getDonVi() {
        return donVi;
    }

    public int getGiaMua() {
        return giaMua;
    }

    public int getGiaBan() {
        return giaBan;
    }
}
