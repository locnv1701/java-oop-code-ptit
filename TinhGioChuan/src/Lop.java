public class Lop {
    private String maLop;
    private String ten;

    public Lop(String maLop, String ten) {
        this.maLop = maLop;
        this.ten = ten;
    }

    public String getMaLop() {
        return maLop;
    }

    public String getTen() {
        return ten;
    }
}
