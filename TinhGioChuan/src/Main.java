import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("MONHOC.in"));
        ArrayList<Lop> dsl = new ArrayList<>();
        ArrayList<GiangVien> dsgv = new ArrayList<>();

        int t = Integer.parseInt(sc.nextLine());
        while (t-- > 0) {
            String[] input = sc.nextLine().split("\\s+");
            String maLop = input[0];
            StringBuilder tenLop = new StringBuilder();
            for (int i = 1; i < input.length; i++) {
                tenLop.append(input[i]);
                tenLop.append(" ");
            }
            dsl.add(new Lop(maLop, tenLop.toString().trim()));
        }
        sc.close();
        sc = new Scanner(new File("GIANGVIEN.in"));
        t = Integer.parseInt(sc.nextLine());
        while (t-- > 0) {
            String[] input = sc.nextLine().split("\\s+");
            String maGV = input[0];
            StringBuilder tenGV = new StringBuilder();
            for (int i = 1; i < input.length; i++) {
                tenGV.append(input[i]);
                tenGV.append(" ");
            }
            dsgv.add(new GiangVien(maGV, tenGV.toString().trim()));
        }
        sc.close();
        sc = new Scanner(new File("GIOCHUAN.in"));
        t = Integer.parseInt(sc.nextLine());
        ArrayList<String> ds = new ArrayList<>();
        while (t-- > 0)
            ds.add(sc.nextLine());

        for (GiangVien x : dsgv) {
            System.out.print(x.getTen() + " ");
            float m = 0;
            for (String s : ds) {
                String[] input = s.split("\\s+");
                float soGio = Float.parseFloat(input[2]);
                if (x.getMaGV().equals(input[0]))
                    m = m + soGio;
            }
            System.out.printf("%.2f\n", m);
        }
    }
}

