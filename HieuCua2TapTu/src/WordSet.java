import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class WordSet {
    private final String fileName;

    public WordSet(String fileName) {
        this.fileName = fileName;
    }

    private Set<String> createSet() throws FileNotFoundException {
        Scanner sc = new Scanner(new File(fileName));
        ArrayList<String> arrayList = new ArrayList<>();
        while (sc.hasNextLine()) {
            String line = sc.nextLine().trim();
            String[] words = line.toLowerCase().split("\\s+");
            arrayList.addAll(Arrays.asList(words));
        }
        return new HashSet<>(arrayList);
    }

    public String difference(WordSet s2) throws FileNotFoundException {
        ArrayList<String> res = new ArrayList<>();
        for (String s : createSet()) {
            if (!s2.createSet().contains(s))
                res.add(s);
        }
        res.sort(null);
        StringBuilder result = new StringBuilder();
        for (String re : res) {
            result.append(re);
            result.append(" ");
        }
        return result.toString();
    }
}
