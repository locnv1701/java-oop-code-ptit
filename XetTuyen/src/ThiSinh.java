import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ThiSinh {
    private String maTS;
    private String ten;
    private Date ngaySinh;
    private double diem1;
    private double diem2;

    public ThiSinh(int maTS, String ten, String ngaySinh, double diem1, double diem2) throws ParseException {
        this.maTS = String.format("PH%02d", maTS);
        this.ten = ten;
        this.ngaySinh = new SimpleDateFormat("dd/MM/yyyy").parse(ngaySinh);
        this.diem1 = diem1;
        this.diem2 = diem2;
    }

    private double diemThuong() {
        if (diem1 >= 8 && diem2 >= 8)
            return 1;
        else if (diem1 >= 7.5 && diem2 >= 7.5)
            return 0.5;
        else return 0;
    }

    private int tinhDiem() {
        int diem = (int) Math.round(((diem1 + diem2) / 2 + diemThuong()) * 10 / 10.0);
        if (diem >= 10)
            diem = 10;
        return diem;
    }

    public String chuanHoa(String hoTen) {
        String[] hoTenArr = hoTen.trim().split("\\s+");
        StringBuilder hoTenSb = new StringBuilder();

        for (String ten : hoTenArr) {
            hoTenSb.append(ten.substring(0, 1).toUpperCase()).append(ten.substring(1).toLowerCase()).append(" ");
        }

        return hoTenSb.toString().trim();
    }

    private String xepLoai() {
        if (tinhDiem() == 9 || tinhDiem() == 10)
            return "Xuat sac";
        else if (tinhDiem() == 8)
            return "Gioi";
        else if (tinhDiem() == 7)
            return "Kha";
        else if (tinhDiem() == 5 || tinhDiem() == 6)
            return "Trung binh";
        return "Truot";
    }

    @Override
    public String toString() {
        return maTS + " " + chuanHoa(ten) + " " + (121 - ngaySinh.getYear()) + " " + tinhDiem() + " " + xepLoai();
    }
}
