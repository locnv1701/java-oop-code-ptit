import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        ArrayList<Employee> employeeArrayList = new ArrayList<>();
        while(t-->0){
            String tmp = sc.nextLine().replace("\\s+", " ").trim();
            String idString = tmp.substring(0,7);
            String name = tmp.substring(8).trim();
            Employee employee = new Employee(idString, name);
            employeeArrayList.add(employee);
        }
        employeeArrayList.sort(null);
        int t2 = Integer.parseInt(sc.nextLine());
        while(t2-->0){
            String find = sc.nextLine().toLowerCase();
            for (Employee employee : employeeArrayList) {
                if(employee.getName().toLowerCase().contains(find)){
                    System.out.println(employee);
                }
            }
            System.out.println();
        }
    }
}
