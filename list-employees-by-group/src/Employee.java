public class Employee implements Comparable<Employee> {
    private String name;
    private String position;
    private String coefficientsSalary;
    private String id;

    public String getName() {
        return name;
    }

    public Employee(String idString, String name) {
        this.name = name;

        String role = idString.substring(0, 2);
        int x = Integer.parseInt(idString.substring(4, 7));

        if (role.equals("GD")) {
            if (x>1) role = "NV";
        }

        if (role.equals("TP") || role.equals("PP")) {
            if (x>3) role = "NV";
        }

        this.position = role;
        this.coefficientsSalary = idString.substring(2, 4);
        this.id = idString.substring(4, 7);
    }

    @Override
    public int compareTo(Employee o) {
        if (coefficientsSalary.compareTo(o.coefficientsSalary) == 0) {
            return id.compareTo(o.id);
        }
        return o.coefficientsSalary.compareTo(coefficientsSalary);
    }

    public String getPosition() {
        return position;
    }

    @Override
    public String toString() {
        return name + ' ' + position + " " + id + " " + coefficientsSalary;
    }
}
