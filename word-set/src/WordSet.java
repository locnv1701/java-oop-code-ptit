import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class WordSet {
    private String fileName;
    private String quinn = "";
    public WordSet(String fileName) {
        this.fileName = fileName;
    }

    private String solve() throws FileNotFoundException {
        File file = new File(fileName);
        Scanner output = new Scanner(file);
        Set<String> set = new HashSet<>();
        while (output.hasNext()) {
            String word = output.next().toLowerCase();
            set.add(word);
        }
        List<String> res = new ArrayList<>(set);
        Collections.sort(res);
        for (String a : res) {
            quinn+= a;
            quinn+="\n";
        }
        return quinn;
    }

    @Override
    public String toString() {
        try {
            solve();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return quinn;
    }
}
