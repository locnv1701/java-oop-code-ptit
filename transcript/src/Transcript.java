
public class Transcript implements Comparable<Transcript> {
    private String id;
    private String name;
    private String grade;
    private double point1;
    private double point2;
    private double point3;

    public Transcript(String id, String name, String grade, double point1, double point2, double point3) {
        this.id = id;
        this.name = name;
        this.grade = grade;
        this.point1 = point1;
        this.point2 = point2;
        this.point3 = point3;
    }

    @Override
    public String toString() {
        return id + ' ' + name + ' ' + grade + ' ' + String.format("%.1f", point1) + ' ' + String.format("%.1f", point2) + ' ' + String.format("%.1f", point3);
    }

    @Override
    public int compareTo(Transcript o) {
        return name.compareTo(o.name);
    }
}
