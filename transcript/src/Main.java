import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws ParseException {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        ArrayList<Transcript> listTranscript = new ArrayList<>();
        while (t-- > 0) {
            Transcript transcript = new Transcript(sc.nextLine(), sc.nextLine(),sc.nextLine(), Double.parseDouble(sc.nextLine()),Double.parseDouble(sc.nextLine()),Double.parseDouble(sc.nextLine()));
            listTranscript.add(transcript);
        }
        listTranscript.sort(null);
        int count = 0;
        for (Transcript transcript : listTranscript) {
            count++;
            System.out.print(count + " ");
            System.out.println(transcript);
        }
    }
}
