import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        ArrayList<Intern> listIntern = new ArrayList<>();
        int count = 0;
        while (t-- > 0) {
            count += 1;
            Intern intern = new Intern(count, sc.nextLine(), sc.nextLine(), sc.nextLine(), sc.nextLine(), sc.nextLine());
            listIntern.add(intern);
        }
        listIntern.sort(null);
        int t2 = Integer.parseInt(sc.nextLine());
        while (t2-->0){
            String entrprise = sc.nextLine();
            for(Intern intern : listIntern){
                if(intern.getEntrprise().equals(entrprise)){
                    System.out.println(intern);
                }
            }
        }
    }
}
