public class Intern implements Comparable<Intern> {
    private int count;
    private String id;
    private String name;
    private String grade;
    private String gmail;
    private String entrprise;


    public Intern(int count, String id, String name, String grade, String gmail, String entrprise) {
        this.count = count;
        this.id = id;
        this.name = name;
        this.grade = grade;
        this.gmail = gmail;
        this.entrprise = entrprise;
    }

    public String getEntrprise() {
        return entrprise;
    }

    @Override
    public int compareTo(Intern o) {
        return id.compareTo(o.id);
    }

    @Override
    public String toString() {
        return count + " " + id + " " + name + " " + grade + " " + gmail + " " + entrprise;
    }
}
