import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        ArrayList<SalaryTeacher> salaryTeachers = new ArrayList<>();
        Map<String, Integer> map = new HashMap<>();
        while (t-- > 0) {
            String ma = sc.nextLine();
            String ten = sc.nextLine();
            int luong = Integer.parseInt(sc.nextLine());
            String r = ma.substring(0, 2);
            if (!map.containsKey(r)) {
                map.put(r, 1);
            } else map.put(r, map.get(r) + 1);
            if (r.equals("HT") && map.get(r) > 1)
                continue;
            if (r.equals("HP") && map.get(r) > 2)
                continue;

            SalaryTeacher salaryTeacher = new SalaryTeacher(ma, ten, luong);
            salaryTeachers.add(salaryTeacher);
        }
        salaryTeachers.forEach(SalaryTeacher::display);
    }
}
