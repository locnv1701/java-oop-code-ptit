public class SalaryTeacher {
    private String id;
    private String name;
    private int basicSalary;

    public SalaryTeacher(String id, String name, int basicSalary) {
        this.id = id;
        this.name = name;
        this.basicSalary = basicSalary;
    }

    public void display() {
        String tmp = id.substring(0, 2);
        int bonus = 0;
        if (tmp.equals("HT"))
            bonus = 2000000;
        else if (tmp.equals("HP"))
            bonus = 900000;
        else
            bonus = 500000;
        int lv = Integer.parseInt(id.substring(2, 4));
        int salary = lv * basicSalary + bonus;
        System.out.println(id + " " + name + " " + lv + " " + bonus + " " + salary);

    }


}
