
public class WordAndCount implements Comparable<WordAndCount> {
    private Integer word;
    private int count;

    public WordAndCount(Integer word, int count) {
        this.word = word;
        this.count = count;
    }

    @Override
    public String toString() {
        return word + " " + count;
    }

    @Override
    public int compareTo(WordAndCount o) {
        return word.compareTo(o.word);
    }
}
