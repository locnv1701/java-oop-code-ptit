import java.io.File;
import java.io.IOException;
import java.util.*;

public class SoKhacNhauTrongFile1 {
    public static void main(String[] args) throws IOException {
        File file = new File("DATA.in");
        Scanner output = new Scanner(file);
        Map<Integer, Integer> map = new HashMap<>();
        while (output.hasNextInt()) {
            Integer a = output.nextInt();
            if (map.containsKey(a))
                map.put(a, map.get(a) + 1);
            else map.put(a, 1);
        }
        ArrayList<WordAndCount> wordList = new ArrayList<>();
        for(Integer digit : map.keySet()){
            WordAndCount wordAndCount = new WordAndCount(digit, map.get(digit));
            wordList.add(wordAndCount);
        }
        wordList.sort(null);
        wordList.forEach(System.out::println);
    }
}
