import static java.lang.Math.round;

public class ThiSinh implements Comparable<ThiSinh> {
    private String ma;
    private String ten;
    private Double diem;
    private Double diemCong;

    public ThiSinh(String ma, String ten, double diem) {
        if (ma.charAt(2) == '1')
            this.diemCong = 0.5;
        if (ma.charAt(2) == '2')
            this.diemCong = 1.0;
        if (ma.charAt(2) == '3')
            this.diemCong = 2.5;
        this.ma = ma;
        this.ten = ten;
        this.diem = diem + diemCong;
    }

    public Double getDiem() {
        return diem;
    }

    @Override
    public int compareTo(ThiSinh o) {
        if (o.diem.compareTo(diem) != 0)
            return o.diem.compareTo(diem);
        return ma.compareTo(o.ma);
    }

    public void display() {
        System.out.print(ma + " " + ten + " ");
        if (diemCong == round(diemCong))
            System.out.print(round(diemCong));
        else System.out.print(diemCong);
        if (diem == round(diem))
            System.out.print(" " + round(diem));
        else System.out.print(" " + diem);
    }
}
