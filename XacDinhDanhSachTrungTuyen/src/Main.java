import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        ArrayList<ThiSinh> dsts = new ArrayList<>();
        while (t-- > 0) {
            dsts.add(new ThiSinh(
                    sc.nextLine(),
                    sc.nextLine(),
                    Double.parseDouble(sc.nextLine()) * 2 +
                            Double.parseDouble(sc.nextLine()) +
                            Double.parseDouble(sc.nextLine())
            ));
        }
        int ct = Integer.parseInt(sc.nextLine());
        dsts.sort(null);
        double diem = dsts.get(ct-1).getDiem();
        System.out.println(diem);
        dsts.forEach(x -> {
            if(x.getDiem() >= diem){
                x.display();
                System.out.println(" TRUNG TUYEN");
            }
            else {
                x.display();
                System.out.println(" TRUOT");
            }
        });
    }
}
